#!/bin/bash
# Script for install WINE (old version without license file)
# Exsample:  make_install_script.sh XXXX-XXXX Local|Network ALTLinux/2.3

WORKDIR=/var/ftp/pvt/Etersoft

. $WORKDIR/config.in

DIST_PATH=~/Document/working
CODE=$1
TYPE_PRODUCT=$2
DIST=$3

exit_handler(){
    local rc=$?
    trap - EXIT
    rm -r  "$TMPDIR_WINE"
    exit $rc
}

fatal(){

    echo ERROR: $1
    exit
}

trap exit_handler EXIT HUP INT QUIT PIPE TERM

echo "������� ��� $DIST ���  $CODE_CLIENT"
mkdir -p $DIST_PATH/$DIST

NAME_DISTR=`echo ${DIST/\//-} | tr [A-Z] [a-z]`
NAME_ARCH=wine-full-$WINEBASEVER-$NAME_DISTR-$CODE

TMPDIR_WINE=`mktemp -t -d make_wine.XXXXXX` || exit 1
mkdir -p $TMPDIR_WINE/$NAME_ARCH || fatal "Can't create temp directory"

# �������� ��������� �����
#cp $WINEPUB_PATH-$WINENUMVERSION/WINE/$DIST/*.* $TMPDIR_WINE/$NAME_ARCH || fatal "Don't found free part $TYPE_PRODUCT- $WINENUMVERSION"
cp $WINEPUB_PATH/WINE/$DIST/*.* $TMPDIR_WINE/$NAME_ARCH || fatal "Don't found free part $TYPE_PRODUCT- $WINENUMVERSION"

# �������� �������� �����
cp $WINEETER_PATH-$WINENUMVERSION-$TYPE_PRODUCT/$CODE/WINE/$DIST/*.* $TMPDIR_WINE/$NAME_ARCH || fatal "Don't found $TYPE_PRODUCT-$CODE"

# �������� ������ ������� WINE  � ������, ����������� ��� ���������
cd  $TMPDIR_WINE/$NAME_ARCH

LIST_ALL_PACK=`find ./   -name "*.rpm" -o  -name "*.tgz" -o  -name "*.tbz" -o -name "*.deb"  -type f | grep -v hasp | xargs echo`
LIST_HASP_PACK=`find ./ -name "*haspd*" -type f | xargs echo`

# �������� ������ ���������� 
cp $WORKDIR/install_wine_tpl.sh $TMPDIR_WINE/$NAME_ARCH/install.sh ||  fatal "Don't found script install"

# �������� ��� ���� ��� ���������� �����. ������� ���������� �� wine
TYPE_PACKAGE=` find ./ -name "wine*" -type f | xargs echo -n`
TYPE_PACKAGE="${TYPE_PACKAGE:(-3)}"

# �������� �������� ���������� � ������ ���������
subst "s|+LIST_ALL_PACKAGES=|LIST_ALL_PACKAGES=\"$LIST_ALL_PACK\"|g" $TMPDIR_WINE/$NAME_ARCH/install.sh
subst "s|+LIST_HASP_PACKAGES=|LIST_HASP_PACKAGES=\"$LIST_HASP_PACK\"|g" $TMPDIR_WINE/$NAME_ARCH/install.sh
subst "s|+CODE_PRODUCT=|CODE_PRODUCT=$CODE|g"  $TMPDIR_WINE/$NAME_ARCH/install.sh
subst "s|+TYPE_PACKAGE=|TYPE_PACKAGE=$TYPE_PACKAGE|g"  $TMPDIR_WINE/$NAME_ARCH/install.sh



# ������� �����
makeself.sh --notemp $TMPDIR_WINE/$NAME_ARCH $DIST_PATH/$DIST/$NAME_ARCH.run \
    	"Uncompressing WINE@Etersoft $WINEVERSION"   || exit 1

rm -rf  $TMPDIR_WINE
