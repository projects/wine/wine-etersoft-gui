#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os, sys
import gettext, locale
sys.path.append('./wineGUI_module/')
sys.path.append('./wineGUI_module/pygtk/')
import gtk.glade
import wineGUI_config
import wineGUI_lang
import wineGUI_win_main

# Вычитываем конфигурацию
config = wineGUI_config.wineGUI_config("./config")

# Заполняем каталоги
my_path = os.getcwd()
config.set("glade_path", my_path + "/wineGUI_glade/")
config.set("module_path", my_path + "/wineGUI_module/")
config.set("lang_path", my_path + "/lang/")
config.set("help_path", my_path + "/win_help/")
config.set("test_path", my_path + "/diagnostics/")

# Сохраняем текущую кодировку
locale.setlocale(locale.LC_ALL, "")
code = locale.nl_langinfo(locale.CODESET)
if code == None:
	code = ""

config.set("code", code)


lang = config.get("lang")
loc = config.get("locale")
locs = []
locs.append(loc)

try:
	locale.setlocale(locale.LC_MESSAGES, loc)
except:
	loc = ""

while True:
	# Задаем локаль, если это необходимо
	if lang=="" or loc=="":
		ret = wineGUI_lang.choice_language("./lang", "./wineGUI_glade")
		if ret==["default", "default"]:
			loc=""
			lang=""
			break
		else:
			lang=ret[0]
			locs=ret[1]
			loc = locs[0]
	
	# Проверяем валидность локали
	err = True
	if lang!="" and loc!="":
		for loc in locs:
			try:
				locale.setlocale(locale.LC_MESSAGES, loc)
				# Устанавливаем язык для GTK
				gtk.glade.bindtextdomain("wineGUI", config.get("lang_path"))
				gtk.glade.textdomain("wineGUI")
				err = False
				break
			except:
				pass
		if err:
			text = "Error: can not set language, error locales:\n"
			for text_loc in locs:
				text = text + text_loc + "\n"
			text = text + "Do you want select other language? If NO, thet will be set default (English)"
			win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_QUESTION, gtk.BUTTONS_YES_NO, text)
			ret = win.run()
			win.destroy()
			lang = ""
			loc = ""
			# Если не хотим ниче удалять, то выходим
			if ret!=-8:
				break
		else:
			break

config.set("lang", lang)
config.set("locale", loc)

# Устанавливаем язык для питона
if lang!="" and loc!="":
	try:
		f = open(config.get("lang_path") + lang + "/LC_MESSAGES/wineGUI.mo", "rb")
		messages = gettext.GNUTranslations(f)
		f.close()
	except:
		messages = gettext.NullTranslations()
	translate = messages.ugettext
	_ = messages.ugettext
else:
	messages = gettext.NullTranslations()
	translate = messages.ugettext
	_ = messages.ugettext

config.translate = translate

# Проверяем, зашли мы от рута или др. юзера
if os.getenv('USER')=="root" or os.getuid()==0:
	config.set("type_root", "root")
	text = "<b>" + _("Warning! You are System Administrator!\n You can Install and Remove Wine, but not recomended run Wine!") + "</b>"
	win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_OK, "")
	win.set_markup(text)
	win.run()
	win.destroy()
# Проверяем, есть ли в системе kdesu или gtksu
elif os.system("which kdesu 1>/dev/null 2>/dev/null")==0:
	config.set("type_root", "kdesu")
elif os.system("which gtksu 1>/dev/null 2>/dev/null")==0:
	config.set("type_root", "gtksu")
elif os.system("which gksu 1>/dev/null 2>/dev/null")==0:
	config.set("type_root", "gksu")
else:
	config.set("type_root", "none")

# Определяем, есть ли в системе apt-get или yum
if os.system("which apt-get 1>/dev/null 2>/dev/null")==0:
	config.set("type_install", "apt-get")
elif os.system("which yum 1>/dev/null 2>/dev/null")==0:
	config.set("type_install", "yum")
else:
	config.set("type_install", "")
	
# Определяем программу консоли
con = os.getenv('TERM')
if con==None:
	if os.system("which xterm 1>/dev/null 2>/dev/null")==0:
		config.set("type_console", "xterm")
	elif os.system("which konsole 1>/dev/null 2>/dev/null")==0:
		config.set("type_console", "konsole")
	else:
		config.set("type_console", "")
else:
	if con in ["xterm", "konsole"]:
		config.set("type_console", con)
	else:
		config.set("type_console", "")

config.write_config()

# Открываем главное окно
win = wineGUI_win_main.main_window(config)
win.run()
