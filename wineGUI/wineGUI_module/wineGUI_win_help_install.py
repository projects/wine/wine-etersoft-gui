#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gtk, gtk.glade
import gobject
import os
import codecs

# Класс окна помощи для установки win-приложений

class win_help_install:
	
	def __init__(self, config):
		self.config = config
		self._ = self.config.translate
		
		self.xml = gtk.glade.XML(config.get("glade_path")+"win_help_install.glade")
		self.xml.signal_autoconnect(self)
		self.win_main = self.xml.get_widget("win_help_install")

		(self.coder, self.decoder) = codecs.lookup("koi8-r")[0:2]

		# Окно программ
		self.tree_prog = self.xml.get_widget("treeview")
		self.model = gtk.TreeStore(gobject.TYPE_STRING)
		self.tree_prog.set_model(self.model)
		column = gtk.TreeViewColumn(self._("Document name"), gtk.CellRendererText(), text=0)
		self.tree_prog.append_column(column)
		
		# Окно подсказки
		self.buf_help = self.xml.get_widget("textview").get_buffer()
		self.buf_help.set_text(self._("Please, select Document from list"))

		# словарь подсказок. Ключ - тип программы , значение - [Имя программы, имя файла]
		self.all_help = {}

		# Составляем список программ, для которых есть помощь
		help_dir = self.config.get("help_path")
		for item in os.listdir(help_dir):
			try:
				f = open(help_dir + "/" + item + "/about", "r")
				#name = self.decoder(f.readline()[:-1:])[0]
				name = f.readline()[:-1:]
				filename = f.readline()
				f.close()
				if name!="" and filename!="":
					filename = help_dir + "/" + item + "/"+ filename
					if os.access(filename, os.F_OK | os.R_OK)==1:
						self.all_help[name] = filename
			except:
				continue
		
		# Пакуем имена в список
		for name in self.all_help.keys():
			row = self.model.insert_before(None, None)
			self.model.set_value(row, 0, self.decoder(name)[0])


		self.win_main.show_all()
		
	def run(self):
		gtk.main()

#___________________________________________________________________________________________
# Обработка событий

	def on_button_exit(self, but, param=None):
		self.win_main.destroy()
		gtk.main_quit()

	def select_prog(self, wid):
		tree = self.tree_prog.get_selection()
		model, iter_prog = tree.get_selected()
		text = ""
		if iter_prog:
			key = self.coder(model.get_value(iter_prog, 0))[0]
			try:
				f = open(self.all_help[key], "r")
				for line in f.readlines():
					text = text + self.decoder(line)[0]
				f.close()
				self.buf_help.set_text(text)
			except:
				self.buf_help.set_text(self._("Sorry, can not show this help file: ") + self.all_help[key])
			
			
			
