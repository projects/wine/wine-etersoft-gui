#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gtk, gtk.glade
import gobject
import popen2
import codecs
import wineGUI_wineadmin_config
import wineGUI_win_edit_disk
import wineGUI_win_show_log as Show_log
import wineGUI_functions_pkg
import wineGUI_help_text

# Класс окна конфигурации

class win_config_wine:

	def __init__(self, config):
		self.config = config
		self._ = self.config.translate

		self.xml = gtk.glade.XML(config.get("glade_path")+"win_config_wine.glade")
		self.xml.signal_autoconnect(self)
		self.win_main = self.xml.get_widget("win_config_wine")
		
		# Отображаем установленную версию wine
		wine_consist = wineGUI_functions_pkg.is_wine_install()
		if wine_consist[0]>0:
			self.xml.get_widget("label_wine").set_markup("<b><span size='12000'>Wine@Etersoft " + wine_consist[2] + " v." + wine_consist[1] + " " + self._("Installed") + "</span></b>")
		else:
			self.xml.get_widget("label_wine").set_markup("<b><span size='12000'>Wine@Etersoft " + self._("not install") + "</span></b>")
		
		# Перекодировщик
		ret = codecs.lookup(self.config.get("code"))
		self.decoder = ret[1]
		
		
		# Окно подсказок
		self.buf_help = self.xml.get_widget("text_help").get_buffer()
		self.help_time=None
		self.TIME_FOR_HELP = 500
		self.TIME_FOR_DOC = 5000
		
		text = wineGUI_help_text.text_win_conf["DOC"]
		self.buf_help.set_text(self._(text))
		
		self.win_main.show_all()
		
		
	def run(self):
		gtk.main()

#___________________________________________________________________________________________
# Обработчики кнопок

	def on_but_exit(self, but, param=None):
		self.win_main.destroy()
		gtk.main_quit()
		

	def on_but_wineadmin(self, but):
		self.win_main.hide()
		wineGUI_wineadmin_config.create_admin(self.config)
		self.win_main.show_all()

	def on_but_pulic_res(self, but):
		string = "<b><span foreground='blue'>" + self._("Configure Default public resurces:") + "</span></b>\n"
		string = string + self._(wineGUI_help_text.text_win_conf_edit_disk)
		string = string + "\n<b><span foreground='blue'>" + self._("Do you want to use it?") + "\n</span></b>"
		win = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_QUESTION, gtk.BUTTONS_YES_NO, "")
		win.set_markup(string)
		ret = win.run()
		win.destroy()
		# Если согласились, запускаем функцию создания общих ресурсов
		if ret==-8:
			self.win_main.hide()
			win = wineGUI_win_edit_disk.win_edit_disk(self.config)
			win.run()
			self.win_main.show_all()

	def on_but_winecfg(self, but):
		self.win_main.hide()
		self.GTK_update()
		com = popen2.Popen4("winecfg")
		ret = com.wait()
		if ret!=0:
			string = "<b>" + self._("System error: can not run WineCfg utility") + "</b>"
			log = "Command: run winecfg\n"
			log = log + "Error: can not run\n"
			log = log + "Detailes:\n\n"
			for line in com.fromchild.readlines():
				log = log + self.decoder(line)[0]
			Show_log.show_error(self.config, gtk.MESSAGE_ERROR, string, log)
		self.win_main.show_all()
#___________________________________________________________________________________________
# Система подсказок
	def mouse_on(self, wid, param=None):
		if self.help_time!=None:
			gobject.source_remove(self.help_time)
		self.help_time = gobject.timeout_add(self.TIME_FOR_HELP, self.show_help, wid.name)
	
	def mouse_off(self, wid, param=None):
		if self.help_time!=None:
			gobject.source_remove(self.help_time)
		self.help_time = gobject.timeout_add(self.TIME_FOR_DOC, self.show_help, "DOC")

	def show_help(self, name):
		self.help_time=None
		self.buf_help.set_text(self._(wineGUI_help_text.text_win_conf[name]))
		return False

#___________________________________________________________________________________________
# Для обновления окна

	def wait_events(self):
		if gtk.events_pending()==0:
			gtk.main_quit()
			return False
		return True
	
	def GTK_update(self):
		gobject.timeout_add(200, self.wait_events)
		gtk.main()



