#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os, pwd
sys.path.append('./wineGUI_module/')
sys.path.append('./wineGUI_module/pygtk/')
import wineGUI_functions_pkg
import wineGUI_win_show_log as Show_log
import wineGUI_config
import wineGUI_wineadmin_config
import wineGUI_distrib
import wineGUI_win_edit_disk
import wineGUI_help_text
import codecs, locale
import gtk, gtk.glade
import gettext
import gobject

def wait_events():
	if gtk.events_pending()==0:
		gtk.main_quit()
		return False
	return True

def GTK_update():
	gobject.timeout_add(200, wait_events)
	gtk.main()



# Главный скрипт, выполняется от root, вызывается через kdesu
# Параметры передаются из командной строки в следующей последовательности:
# файл_конфигурации команда тип_пакета имена_пакетов (в порядке установки)...
# Коды возврата анализируются в материнском процессе:
# 3 - отмена действия
# 1 - ошибка инициализации модуля
# 2 - ошибка при выполнении команды
# 0 - все успешно

# Получаем путь к файлу конфигурации
try:
	config_file = sys.argv[1]
except:
	print "Config file not found"
	sys.exit(1)

# Получаем команду
try:
	command = sys.argv[2]
except:
	print "Command not found"
	sys.exit(1)
if command not in ["install", "uninstall", "update", "reinstall", "install_dep", "reinstall_dep"]:
	print "Error comand"
	sys.exit(1)

# Включен ли режим проверки зависимостей
depend = False

if command == "reinstall_dep":
	command = "reinstall"
	depend = True
if command == "install_dep":
	command = "install"
	depend = True
	



# Получаем тип пакета
try:
	type_product = sys.argv[3]
except:
	print "Type product not found"
	sys.exit(1)
if type_product not in ["wine", "hasp", "font"]:
	print "Error Type product"
	sys.exit(1)
if type_product=="wine":
	name_product = "Wine@Etersoft"
elif type_product=="hasp":
	name_product = "HASP driver"
elif type_product=="font":
	name_product = "MS Core Fonts"

# Список пакетов, если он есть
list_file = []
i = 4
try:
	while(True):
		pkg = sys.argv[i]
		list_file.append(pkg)
		i = i + 1
except:
	# конец списска
	pass

if (command in ["update","install","reinstall"]) and list_file==[]:
	print "Command not found"
	sys.exit(1)

# Вскрываем конфигурацию
config = wineGUI_config.wineGUI_config(config_file)

# Перекодировщик
code = config.get("code")
if code == "":
	code = None
ret = codecs.lookup(code)
coder = ret[0]
decoder = ret[1]

# Установка языка
lang = config.get("lang")
loc = config.get("locale")

if lang!="" and loc!="":
	# Устанавливаем язык для питона
	try:
		f = open(config.get("lang_path") + lang + "/LC_MESSAGES/wineGUI.mo", "rb")
		messages = gettext.GNUTranslations(f)
		f.close()
	except:
		messages = gettext.NullTranslations()
	translate = messages.ugettext
	_ = messages.ugettext
	# Устанавливаем язык для GTK
	locale.setlocale(locale.LC_MESSAGES, loc)
	gtk.glade.bindtextdomain("wineGUI", config.get("lang_path"))
	gtk.glade.textdomain("wineGUI")
else:
	messages = gettext.NullTranslations()
	translate = messages.ugettext
	_ = messages.ugettext

config.translate = translate

type_pkg = wineGUI_distrib.get_type_pkg(config.get("linux_distrib"), config.get("module_path"))
if type_pkg=="":
	type_pkg = "rpm"

# Обрабатываем команду
if command == "uninstall":
	# Получаем список пакетов
	win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_NONE, _("Search Packages..."))
	win.show_all()
	GTK_update()
	if type_product=="wine":
		list_pkg = wineGUI_functions_pkg.check_pkg(type_pkg, "wine")
	elif type_product=="hasp":
		list_pkg = wineGUI_functions_pkg.check_pkg(type_pkg, "haspd")
	elif type_product=="font":
		list_pkg = wineGUI_functions_pkg.check_pkg(type_pkg, "fonts-ttf-ms")
	win.destroy()
	# Если возникла ошибка
	if type(list_pkg)==type(" "):
		string = "<b>" + _("System error: can not find and Remove")+" " + name_product + " " + _("packages") + "</b>"
		log = "Command: search old " + type_product + " packages for remove\n"
		log = log + "Error: can not find old " + type_product + " packages\n"
		log = log + "Detailes:\n\n" + decoder(list_pkg)[0]
		Show_log.show_error(config, gtk.MESSAGE_ERROR, string, log)
		sys.exit(2)
	# Если пакетов нет
	if list_pkg==[]:
		string = "<b>" + _("Error: Can not find") + " " + name_product + " " + _("packages!") + "</b>"
		win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, "")
		win.set_markup(string)
		win.run()
		win.destroy()
		sys.exit(2)
	string = "<b>" + _("Warning! Next packages will be removed") + ":</b>\n\n"
	for line in list_pkg:
		string = string + "<b><span foreground='blue'>" + line + "</span></b>\n"
	string = string + "\n<b><span foreground='red'>" + _("Remove it?") + "</span></b>"
	win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_QUESTION, gtk.BUTTONS_YES_NO, "")
	win.set_markup(string)
	ret = win.run()
	win.destroy()
	# Если не хотим ниче удалять, то выходим
	if ret!=-8:
		sys.exit(3)
	# Удаляем старые пакеты
	win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_NONE, _("Removing packages..."))
	win.show_all()
	GTK_update()
	ret = wineGUI_functions_pkg.remove_pkg(type_pkg, list_pkg)
	win.destroy()
	# Если какие-то пакеты не удалось удалить, выдаем их список и выходим
	if ret[0]!=[]:
		string = "<b>" + _("Error! Can not remove") + " " + name_product + "!</b>"
		log = "Command: remove old " + type_product + "packages\n"
		log = log + "Error: Can not remove\n"
		log = log + "Detailes:\n\n"
		log = log + "Can not remove next packages:\n"
		for pkg in ret[0]:
			log = log + pkg + "\n"
		log = log + "\n"
		log = log + "Errors:\n"
		for line in ret[1]:
			if line!="" and line!="\n":
				log = log + decoder(line)[0]
			log = log + "\n"
		Show_log.show_error(config, gtk.MESSAGE_ERROR, string, log)
		sys.exit(2)
	# Если какие-то ошибки, выдаем их список и выходим
	if ret[0]==[] and ret[1]!=[]:
		string = "<b>" + _("Some errors, but all packages removed!") + "</b>"
		log = "Command: remove old " + type_product + "packages\n"
		log = log + "Error: remove, but with errors!\n"
		log = log + "Detailes:\n\n"
		for line in ret[1]:
			if line!="" and line!="\n":
				log = log + decoder(line)[0]
			log = log + "\n"
		Show_log.show_error(config, gtk.MESSAGE_INFO, string, log)
		sys.exit(0)
	# Если все ОК, выдаем сообщение об этом
	string = "<b><span foreground='blue'>" + name_product + " " + _("Removed successfully") + "</span></b>"
	win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_OK, "")
	win.set_markup(string)
	win.run()
	win.destroy()
	sys.exit(0)

elif command=="install" or command=="reinstall":
	# Проверяем, установлены ли какие либо пакеты wine
	win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_NONE, _("Search old packages..."))
	win.show_all()
	GTK_update()
	if type_product=="wine":
		list_pkg = wineGUI_functions_pkg.check_pkg(type_pkg, "wine")
	elif type_product=="hasp":
		list_pkg = wineGUI_functions_pkg.check_pkg(type_pkg, "haspd")
	elif type_product=="font":
		list_pkg = wineGUI_functions_pkg.check_pkg(type_pkg, "fonts-ttf-ms")
	win.destroy()
	# Если возникла ошибка
	if type(list_pkg)==type(" "):
		string = "<b>" + _("System error: Can not install") + " " + name_product + "</b>"
		log = "Command: search old " + type_product + " packages for remove and then install new\n"
		log = log + "Error: can not find old " + type_product + " packages\n"
		log = log + "Detailes:\n\n" + decoder(list_pkg)[0]
		Show_log.show_error(config, gtk.MESSAGE_ERROR, string, log)
		sys.exit(2)
	# Если есть неудаленные пакеты
	if list_pkg!=[]:
		string = "<b>" + _("In system found next old packages:") + "</b>\n\n"
		for line in list_pkg:
			string = string + "<b><span foreground='blue'>" + line + "</span></b>\n"
		if command=="install":
			string = string + "\n<b>" + _("For Install") + " " + name_product + " " + _("need remove all!") + "\n<span foreground='red'>" + _("Remove it?") + "</span></b>"
		else:
			string = string + "\n<b>" + _("For Reinstall") + " " + name_product + " " + _("need remove all!") + "\n<span foreground='red'>" + _("Remove it?") + "</span></b>"
		win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_QUESTION, gtk.BUTTONS_YES_NO, "")
		win.set_markup(string)
		ret = win.run()
		win.destroy()
		# Если не хотим ниче удалять, то выходим
		if ret!=-8:
			if command=="install":
				string = "<b>" + _("Can not continue Installation") + " " + name_product + ": " + _("old packages must be removed") + "</b>"
			else:
				string = "<b>" + _("Can not continue Reinstallation") + " " + name_product + ": " + _("old packages must be removed") + "</b>"
			win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, "")
			win.set_markup(string)
			win.run()
			sys.exit(3)
		# Удаляем старые пакеты
		win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_NONE, _("Removing old packages..."))
		win.show_all()
		GTK_update()
		ret = wineGUI_functions_pkg.remove_pkg(type_pkg, list_pkg)
		win.destroy()
		# Если какие-то пакеты не удалось удалить, выдаем их список и выходим
		if ret[0]!=[]:
			string = "<b>" + _("Error! Can not remove old packages!") + "\n"
			if command=="install":
				string = string + _("Can not install") + " " + name_product + "</b>"
			else:
				string = string + _("Can not Reinstall") + " " + name_product + "</b>"
			log = "Command: remove old " + type_product + "packages for install new\n"
			log = log + "Error: Can not remove\n"
			log = log + "Detailes:\n\n"
			log = log + "Can not remove next packages:\n"
			for pkg in ret[0]:
				log = log + pkg + "\n"
			log = log + "\n"
			log = log + "Errors:\n"
			for line in ret[1]:
				if line!="" and line!="\n":
					log = log + decoder(line)[0]
				log = log + "\n"
			Show_log.show_error(config, gtk.MESSAGE_ERROR, string, log)
			sys.exit(2)
		# Если какие-то ошибки, выдаем их список и выходим
		if ret[0]==[] and ret[1]!=[]:
			string = "<b>" + _("Some errors, but all packages removed!") + "\n"
			string = string + _("Press OK for continue") + "</b>"
			log = "Command: remove old " + type_product + "packages for install new\n"
			log = log + "Error: remove, but with errors!\n"
			log = log + "Detailes:\n\n"
			for line in ret[1]:
				if line!="" and line!="\n":
					log = log + decoder(line)[0]
				log = log + "\n"
			Show_log.show_error(config, gtk.MESSAGE_INFO, string, log)
	# Устанавливаем пакеты
	# Если включен режим удовлетворения зависимостей, открываем отдельную консоль
	if depend:
		string = _("Now, will be opened console for instalation, follow the instruction in it!")
		win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_OK, "")
		win.set_markup(string)
		win.run()
		win.destroy()
		GTK_update()

		command = config.get("type_console") + " -e \"(" + config.get("type_install") + " install"
		for pkg in list_file:
			command = command + " " + pkg
		command = command + " && echo '0'>/tmp/winegui.tmp || echo '1'>/tmp/winegui.tmp "
		command = command + ") && echo '" + coder( _("This window will be automatic cloused after 30 seconds. You can clouse it now for continue") )[0] + "' && sleep 30 \""
		os.system(command)
		
		# Обрабатываем код возврата
		ret = "1"
		try:
			f = open("/tmp/winegui.tmp", "r")
			ret = f.readline()
			f.close()
			# Удаляем файл, мусорить не хорошо
			os.remove("/tmp/winegui.tmp")
		except:
			pass
		
		if ret == "":
			ret=="1"
		# Если при установке произошла ошибка
		if ret=="1":
			string = "<b>" + _("Error! Can not install") + " " + name_product + "!</b>"
			win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, "")
			win.set_markup(messages)
			win.show_all()
			win.run()
			win.destroy()
			GTK_update()
			sys.exit(2)
		else:
			# Если нет ошибок
			# Показываем юзеру, какие пакеты были успешно установлены и переходим к следующему шагу установки
			string = "<b><span foreground='blue'>" + name_product + " " + _("successfully installed!") + "</span></b>\n"
			string = string + "<b>" + _("Press OK for continue") + "</b>"
			win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_OK, "")
			win.set_markup(string)
			win.run()
			win.destroy()
		
	else:
		messages = "<b><span size='12000'>" + _("Installing new packages:") + "\n\n</span></b>"
		win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_NONE, "")
		win.set_size_request(450,250)
		win.set_markup(messages)
		win.show_all()
		GTK_update()
		fatal = False
		errors = {}
		for pkg in list_file:
			# Выводить на экран будем только короткое имя
			ind = pkg.rfind("/")
			if ind > 0:
				small = pkg[ind+1::]
			else:
				small = pkg
			win.set_markup(messages + small + "...")
			GTK_update()
			ret = wineGUI_functions_pkg.install_pkg(type_pkg, pkg)
			if ret[0]==0:
				messages = messages + "<b>" + small + ".....<span foreground='blue'>" + _("OK") + "</span></b>\n"
			elif ret[0]==1:
				messages = messages + "<b>" + small + ".....<span foreground='red'>" + _("Some errors") + "</span></b>\n"
			elif ret[0]==2:
				messages = messages + "<b>" + small + ".....<span foreground='red'>" + _("Error!") + "</span></b>\n"
			if ret[0]>0:
				errors[small] = ret
				if ret[0]==2:
					fatal=True
		win.destroy()
		# Если возникла ошибка при инсталяции, выдаем ее
		if errors!={}:
			log = "Command: Install " + name_product + "\n"
			if fatal:
				string = "<b>" + _("Error! Can not install") + " " + name_product + "!</b>"
				log = log + "Error: Can not install\n"
			else:
				string = "<b>" + _("Warning! Installed with some errors!") + "</b>"
				log = log + "Error: installed with errors\n"
			log = log + "Detailes:\n\n"
			for key in errors.keys():
				log = log + "Packege " + key + ":"
				if errors[key][0] == 1:
					log = log + " installed\n"
				else:
					log = log + " not installed:\n"
				for line in errors[key][1]:
					if line!="" and line!="\n":
						log = log + decoder(line)[0]
			if fatal:
				Show_log.show_error(config, gtk.MESSAGE_ERROR, string, log)
			else:
				Show_log.show_error(config, gtk.MESSAGE_INFO, string, log)
			if fatal:
				sys.exit(2)
		else:
			# Если нет ошибок
			# Показываем юзеру, какие пакеты были успешно установлены и переходим к следующему шагу установки
			string = "<b><span foreground='blue'>" + name_product + " " + _("successfully installed!") + "</span></b>\n"
			string = string + "<b>" + _("Press OK for continue") + "</b>"
			win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_OK, "")
			win.set_markup(string)
			win.run()
			win.destroy()
	
	# Если устанавливаем wine, то переходим к настройке
	if type_product=="wine":
		# Будем ли использовать wine-admin
		string = "<b><span foreground='blue'>" + _("Administrate Install win-programs:") + "</span></b>\n"
		string = string + _(wineGUI_help_text.text_script_admin)
		string = string + "\n<b><span foreground='blue'>" + _("Do you want to use it?") + "\n</span></b>"
		string = string + _("You can start this mode from Config in the future") + "\n"
		win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_QUESTION, gtk.BUTTONS_YES_NO, "")
		win.set_markup(string)
		ret = win.run()
		win.destroy()
		# Если согласились, запускаем функцию создания юзера-администратора
		if ret==-8:
			wineGUI_wineadmin_config.create_admin(config)
		# Будем ли создавать общие диски
		string = "<b><span foreground='blue'>" + _("Configure Default public resurces:") + "</span></b>\n"
		string = string + _(wineGUI_help_text.text_script_disk)
		string = string + "\n<b><span foreground='blue'>" + _("Do you want to use it now?") + "\n</span></b>"
		string = string + _("You can start this mode from Config in the future") + "\n"
		win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_QUESTION, gtk.BUTTONS_YES_NO, "")
		win.set_markup(string)
		ret = win.run()
		win.destroy()
		# Если согласились, запускаем функцию создания общих ресурсов
		if ret==-8:
			win = wineGUI_win_edit_disk.win_edit_disk(config)
			win.run()
		
		# Завершающий этап
		string = "<b><span foreground='blue'>" + _("Installations is finished successfully!") + "</span></b>\n"
		string = string + _(wineGUI_help_text.text_script_finish)
		win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_OK, "")
		win.set_markup(string)
		ret = win.run()
		win.destroy()
	
	sys.exit(0)

elif command=="update":
	messages = _("Update") + " " + name_product + " " + _("packages...")
	win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_NONE, messages)
	#win.set_size_request(450,250)
	#win.set_markup(messages)
	win.show_all()
	GTK_update()
	#fatal = False
	##errors = {}
	#for pkg in list_file:
		## Выводить на экран будем только короткое имя
		##ind = pkg.rfind("/")
		##if ind > 0:
			##small = pkg[ind+1::]
		##else:
			##small = pkg
		##win.set_markup(messages + small + "...")
		##GTK_update()
		#ret = wineGUI_functions_pkg.update_pkg(type_pkg, pkg)
		#if ret[0]==0:
			#messages = messages + "<b>" + small + ".....<span foreground='blue'>OK</span></b>\n"
		#elif ret[0]==1:
			#messages = messages + "<b>" + small + ".....<span foreground='red'>Some errors</span></b>\n"
		#elif ret[0]==2:
			#messages = messages + "<b>" + small + ".....<span foreground='red'>Error!</span></b>\n"
		#if ret[0]>0:
			#errors[small] = ret
			#if ret[0]==2:
				#fatal=True

	ret = wineGUI_functions_pkg.update_pkg(type_pkg, list_file)
	

	win.destroy()
	GTK_update()
	# Если возникла ошибка при инсталяции, выдаем ее
	if ret[0] > 0:
		log = "Command: Update " + name_product + "\n"
		if ret[0]==2:
			string = "<b>" + _("Error! Can not update") + " " + name_product + "!</b>"
			log = log + "Error: Can not update\n"
		else:
			string = "<b>" + _("Warning! Updatede with some errors!") + "\n</b>"
			string = string + _(wineGUI_help_text.text_script_update)
			log = log + "Error: updated with errors\n"
		log = log + "Detailes:\n\n"

		log = log + "Packages for update:\n"
		for pkg in list_file:
			log = log + pkg + "\n"
		log = log + "\nErrors:\n\n"

		#for pkg in list_file:
			#ind = pkg.rfind("/")
			#if ind > 0:
				#small = pkg[ind+1::]
			#else:
				#small = pkg
			#if small not in errors.keys():
				#continue
			#log = log + "Packege " + small + ":"
			#if errors[small][0] == 1:
				#log = log + " updated\n"
			#else:
				#log = log + " not updated:\n"
		for line in ret[1]:
			if line!="" and line!="\n":
				log = log + decoder(line)[0]

		
		if ret[0]==2:
			Show_log.show_error(config, gtk.MESSAGE_ERROR, string, log)
		else:
			Show_log.show_error(config, gtk.MESSAGE_INFO, string, log)
		if ret[0]==2:
			sys.exit(2)
		else:
			sys.exit(0)
	# Если нет ошибок
	# Показываем юзеру, какие пакеты были успешно установлены и переходим к следующему шагу установки
	string = "<b><span foreground='blue'>" + name_product + " " + _("Updated successfully!") + "</span></b>\n"
	string = string + _(wineGUI_help_text.text_script_update)
	win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_OK, "")
	win.set_markup(string)
	win.run()
	win.destroy()
	sys.exit(0)
