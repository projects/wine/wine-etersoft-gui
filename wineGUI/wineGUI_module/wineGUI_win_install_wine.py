#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Первичное окно установки
import gtk, gtk.glade
import wineGUI_distrib
import gobject, os
import wineGUI_win_pkg
import wineGUI_functions_pkg
import wineGUI_help_text
import codecs

class win_install:
	def __init__(self, config):
		self.config = config
		self._ = self.config.translate
		
		self.xml = gtk.glade.XML(config.get("glade_path")+"win_install_wine.glade")
		self.xml.signal_autoconnect(self)
		self.win_main = self.xml.get_widget("win_install")
		
		self.button_next = self.xml.get_widget("but_next")
		
		decoder = codecs.lookup("koi8-r")[1]
		# Выводим тип продукта и версию
		text = "<span size='14000'>Wine@Etersoft " + config.get("wine_name") + ", " + self._("version") + " " + config.get("gui_version") + " " + self._("from") + " " + decoder(config.get("ver_date"))[0] + "</span>"
		self.xml.get_widget("label_product").set_markup(text)
		self.xml.get_widget("label_serial").set_markup("<b>" + self._("Registration number")+ ": " + config.get("serial") + "</b>")
		
		# Создаем боксы с дистрибутивом и версией
		self.ComBox_distrib = gtk.combo_box_new_text()
		self.ComBox_distrib.set_size_request(150, 30)
		self.ComBox_distrib.set_property("name", "combox_distrib")
		self.xml.get_widget("com_distrib").pack_start(self.ComBox_distrib)
		self.ComBox_distrib.show()

		self.ComBox_version = gtk.combo_box_new_text()
		self.ComBox_version.set_size_request(100, 30)
		self.ComBox_version.set_property("name", "combox_version")
		self.xml.get_widget("com_version").pack_start(self.ComBox_version)
		self.ComBox_version.show()
		
		# Количество версий линукса в комбобоксе в данный момент
		self.n_ver = 0
		
		self.ComBox_distrib.connect("changed", self.switch_com)
		self.ComBox_distrib.connect("enter_notify_event", self.mouse_on)
		self.ComBox_distrib.connect("leave_notify_event", self.mouse_off)
		self.ComBox_version.connect("enter_notify_event", self.mouse_on)
		self.ComBox_version.connect("leave_notify_event", self.mouse_off)

		# Разрешаем режим установки с проверкой зависимостей, если в системе есть все что нужно
		self.CheckBbutton = self.xml.get_widget("checkbutton")
		if self.config.get("type_install")!="" and self.config.get("type_console")!="":
			self.CheckBbutton.show()
		else:
			self.CheckBbutton.hide()
		
		# Пытаемся определить текущий дистрибутив, если он не передан конструктору из вне
		# Если не определили, пытаемся взять его из конфигурационного файла
		self.distrib = None
		self.version = None
		distr = wineGUI_distrib.get_distrib(self.config.get("module_path"))
		if distr[0]!='':
			self.distrib = distr[0]
			if distr[1]!='':
				self.version=distr[1]
		else:
			self.distrib = config.get("linux_distrib")
			if self.distrib!="":
					self.version = config.get("linux_version")
		
		# Пакуем известные дистрибутивы и ставим текущий
		# Версии пакуются автоматически при смене состояния бокса дистрибутивов
		n = 0
		ind = None
		self.box_distrib = []
		self.box_version = []
		for item in wineGUI_distrib.all_distrib.keys():
			if self.distrib!="":
				if item==self.distrib:
					ind = n
			self.ComBox_distrib.append_text(item)
			self.box_distrib.append(item)
			n = n + 1
		if ind!=None:
			self.ComBox_distrib.set_active(ind)
		
		# Надписи состояния пакетов
		self.all_consist = ["<b>" + self._("Not install") + "</b>", "<b><span foreground='blue'>" + self._("Installed") + "</span></b>",
"<b><span foreground='darkgreen'>" + self._("will INSTALL") + "</span></b>", "<b><span foreground='red'>" + self._("will REINSTALL") + "</span></b>",
"<b><span foreground='red'>" + self._("will REMOVE") + "</span></b>",
"<b><span foreground='darkgreen'>" + self._("Update to") + " \n" + config.get("wine_name") + " v." + config.get("gui_version") + "</span></b>",
"<b><span foreground='red'>" + self._("will REINSTALL\nto") + " " + config.get("wine_name") + " v." + config.get("gui_version") + "</span></b>",
"<b>" + self._("Undefined") + "</b>", "<b><span foreground='darkgreen'>" + self._("Update or Install") + "</span></b>"]
		
		# Стиль
		BG_COLOR = "#AAEEFF"
		HELP_BG_COLOR = "#AACCFF"
		self.TIME_FOR_HELP = 500
		self.TIME_FOR_DOC = 5000

		# Устанавливаем цвет фона
		#self.set_color(BG_COLOR)
		self.xml.get_widget("viewport").modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse("#E6E6E6"))
		self.xml.get_widget("eventbox_wine").modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse("#E6E6E6"))
		self.xml.get_widget("eventbox_hasp").modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse("#E6E6E6"))
		self.xml.get_widget("eventbox_font").modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse("#E6E6E6"))
		self.xml.get_widget("viewport_help").modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse("black"))

		# Таймер для подсказки
		self.help_time=None
		# Окно подсказок
		self.buf_help = self.xml.get_widget("text_help").get_buffer()
		text = wineGUI_help_text.text_win_install["DOC"]
		self.buf_help.set_text(self._(text))

		# Блокируем кнопку Next, пока не будет выбран дистрибутив
		if self.distrib=="":
			self.button_next.set_sensitive(False)
			self.ComBox_version.set_sensitive(False)

		# Устанавливаем начальные значения
		self.test_system()

		# Выходные данные
		self.ret_val = 0

		self.win_main.show()

	def run(self):
		gtk.main()
		return self.ret_val
	
	def test_system(self):
		# Определяем, установлены ли Wine
		self.wine_consist = wineGUI_functions_pkg.is_wine_install()
		# Если установлен чужой wine, выводим название последней версии
		# Иначе, выводим установленную версию
		self.ready_to_update = False
		if self.wine_consist[0]>0:
			self.xml.get_widget("label_wine").set_markup("<b>Wine@Etersoft " + self.wine_consist[2] + " v." + self.wine_consist[1] + "</b>\n " + self._("Serial number") + ": " + self.wine_consist[3])
			# Определяет, можно ли обновлять продукт
			temp = wineGUI_functions_pkg.is_new(self.wine_consist[1], self.config.get("gui_version"))
			old_name = self.wine_consist[2]
			new_name = self.config.get("wine_name")
			if (temp>0) or (temp==0 and ((old_name=="Free" and new_name!="Free") or \
					(old_name=="Local" and new_name in ["Network", "SQL"]) or \
					 (old_name=="Network" and new_name=="SQL"))):
				self.ready_to_update = True
		else:
			self.xml.get_widget("label_wine").set_markup("<b>Wine@Etersoft " + self.config.get("wine_name") + " v." + self.config.get("gui_version") + "</b>")
		# Определяем, установлены ли драйвера HASP
		self.hasp_consist = wineGUI_functions_pkg.is_hasp_install()
		# Пакеты wine
		self.label_wine = self.xml.get_widget("label_wine_install")
		self.label_wine_n = 0
		
		if self.wine_consist[0]==1 and self.wine_consist[1]==self.config.get("gui_version") and self.wine_consist[2]==self.config.get("wine_name"):
			self.label_wine_n = 1
		else:
			if self.ready_to_update:
				self.label_wine_n = 5
			else:
				if self.wine_consist[0] > 0:
					self.label_wine_n = 6
				else:
					self.label_wine_n = 2
		self.label_wine.set_markup(self.all_consist[self.label_wine_n])
		# Пакеты HASP
		self.label_hasp = self.xml.get_widget("label_hasp_install")
		self.label_hasp_n = 0
		if self.hasp_consist:
			self.label_hasp_n = 1
		else:
			self.label_hasp_n = 0
		self.label_hasp.set_markup(self.all_consist[self.label_hasp_n])
		# Пакеты шрифтов
		self.label_font = self.xml.get_widget("label_font_install")
		self.label_font_n = 7
		self.label_font.set_markup(self.all_consist[self.label_font_n])

	
	
#___________________________________________________________________________________________
# Обработчики кнопок

	# На кнопку Назад и Закрытие окна
	def back(self, name=None, param=None):
		self.win_main.destroy()
		self.ret_val = -1
		gtk.main_quit()

	def next(self, name=None):
		if self.label_hasp_n < 2 and self.label_wine_n < 2 and self.label_font_n==7:
			win = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, self._("No selected action"))
			win.run()
			win.destroy()
			return
		# Продолжаем установку
		# Прячем это окно на случай, если понадобиться вернуться назад
		self.win_main.hide()
		# Если версия не выбрана, устанавливаем первую
		try:
			ver = self.box_version[self.ComBox_version.get_active()]
		except:
			ver = ""
		if ver=="":
			self.ComBox_version.set_active(0)
		# Заполняем конфигурацию без записи на диск
		self.config.set("linux_distrib", self.distrib)
		self.config.set("linux_version", ver)
		# Открываем следующее окно, поочереди для всех компонент
		self.GTK_update()
		for PKG in [{"name":"wine", "value":self.label_wine_n}, {"name":"hasp", "value":self.label_hasp_n},
				{"name":"font", "value":self.label_font_n}]:
			# Если выбрана установка, переустановка или обновление
			if PKG["value"] in [2,3,5,6,8]:
				ret = self.win_qwest(PKG["name"], PKG["value"])
				# Если не хотим ниче удалять, то выходим
				if ret!=-5:
					continue
				win_com = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_NONE, self._("Search packages..."))
				win_com.show_all()
				self.GTK_update()
				# Если включена проверка зависимостей
				val = PKG["value"]
				if PKG["value"] not in [5,8]:
					if self.CheckBbutton.get_active():
						val = PKG["value"] + 10
				win_wine_pkg = wineGUI_win_pkg.win_pkg(self.config, val, PKG["name"])
				win_com.destroy()
				self.GTK_update()
				# Внутри функции run еще один gtk.main()
				ret = win_wine_pkg.run()
				# Если была нажа кнопка назад в окне, то возвращаемся в это окно
				#if ret<0:
				#	self.win_main.show()
				#	return
			# Если выбрано удаление
			elif PKG["value"]==4:
				ret = self.win_qwest(PKG["name"], PKG["value"])
				self.GTK_update()
				# Если не хотим ниче удалять, то выходим
				if ret!=-5:
					continue
				command = self.config.get("module_path")+"wineGUI_script_pkg.py " + os.getcwd() + "/config uninstall " + PKG["name"]
				type_run = self.config.get("type_root")
				if type_run=="root":
					ret = os.system(command)
				elif type_run=="kdesu":
					ret = os.system("kdesu -d -n -c '" + command + "'")
				elif type_run=="gtksu":
					ret = os.system("gtksu '" + command + "'")
				elif type_run=="gksu":
					ret = os.system("gksu '" + command + "'")
				else:
					text = "<b>" + self._("ERROR:\nIn your system not found kdesu, gtksu or gksu") + "\n"
					text = text + self._("For Uninstall packages needs ROOT rights!") + "\n"
					text = text + self._("Please, restart program as ROOT") + "</b>"
					win = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, "")
					win.set_markup(text)
					win.run()
					win.destroy()
					# Закрываем главное окно
					self.win_main.destroy()
					self.ret_val = -1
					gtk.main_quit()
					return
		self.config.write_config()
		win_com = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_NONE, self._("Testing system..."))
		win_com.show_all()
		self.GTK_update()
		self.test_system()
		win_com.destroy()
		self.GTK_update()
		self.win_main.show()

#___________________________________________________________________________________________
# Окно отображающее действие над программой
	
	def win_qwest(self, name, type_do):
		# Название программы
		string = "<b>" + self._("Program") + ":</b>\n<span foreground='blue'>"
		if name=="wine":
			string = string + self.xml.get_widget("label_wine").get_text()
		elif name=="hasp":
			string = string + self.xml.get_widget("label_hasp").get_text()
		elif name=="font":
			string = string + self.xml.get_widget("label_font").get_text()
		# Статус
		if type_do in [2,8]:
			string = string + "</span>\n\n<b>" + self._("Status") + ": </b>" + self._("Not install") + "\n\n"
		else:
			string = string + "</span>\n\n<b>" + self._("Status") + ": </b>" + self._("Installed") + "\n\n"
		
		# Действие
		if type_do==4:
			string = string + self._("Now, this program will be <b>Remove</b> from your system.") + "\n"
		elif type_do in [2,8]:
			string = string + self._("Now, this program will be <b>Install</b> on your system.") + "\n"
		elif type_do==3:
			string = string + self._("Now, this program will be <b>Reinstall</b>.") + "\n"
		elif type_do==6:
			string = string + self._("Now, this program will be <b>Reinstall</b> to Wine@Etersoft") + " " + self.config.get("wine_name") + " v." + self.config.get("gui_version") + ".\n"
		elif type_do==5:
			string = string + self._("Now, this program will be <b>Update</b> to Wine@Etersoft") + " " + self.config.get("wine_name") + " v." + self.config.get("gui_version") + ".\n"

		string = string + self._("Press OK for continue or Cancel")

		win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_QUESTION, gtk.BUTTONS_OK_CANCEL, "")
		win.set_markup(string)
		ret = win.run()
		win.destroy()
		
		return ret

#___________________________________________________________________________________________
# Срабатывает при смене дистрибутива
	def switch_com(self, wid):
		try:
			self.distrib = self.box_distrib[self.ComBox_distrib.get_active()]
		except:
			self.distrib = ""
		self.button_next.set_sensitive(True)
		self.ComBox_version.set_sensitive(True)
		# Удаляем старые версии
		n=0
		while(n<self.n_ver):
			self.ComBox_version.remove_text(0)
			n = n + 1
		# Пакуем известные версии
		n = 0
		ind = -1
		self.n_ver = 0
		self.box_version = []
		for item in wineGUI_distrib.all_distrib[self.distrib]:
			if self.version!=None:
				if self.version==item:
					ind = n
			self.ComBox_version.append_text(item)
			self.box_version.append(item)
			self.n_ver = self.n_ver + 1
			n = n + 1
		if ind!=-1:
			self.ComBox_version.set_active(ind)

#___________________________________________________________________________________________
# Срабатывает при щелчке по дистрибутиву
	def switch_consist(self, wid, param=None):
		if wid.name == "eventbox_wine":
			n = self.label_wine_n
		elif wid.name == "eventbox_hasp":
			n = self.label_hasp_n
		elif wid.name == "eventbox_font":
			n = self.label_font_n
		# Если пакет не был установлен, то будет установлен
		if n==0:
			n = 2
		# Если установлен, то будет Переустанволен или обновлен
		elif n==1:
			if wid.name == "eventbox_wine":
				if self.ready_to_update:
					n = 5
				else:
					n = 6
			else:
					n = 3
		# Eсли указано Установить, то поменять на Не установлен
		elif n==2:
			n=0
		# Если указано Переустановить, то заменяем на Удалить
		elif n==3 or n==6:
			n=4
		# Если указано Удалить, то меняем на Установлен
		elif n==4:
			# Шрифты меняем на Не определено
			if wid.name == "eventbox_font":
				n = 7
			else:
				n=1
		# Если Обновить, то Переустановить на новую версию
		elif n==5:
			n=6
		# Если Не определено, то Обновить
		elif n==7:
			n=8
		# Если Обновить, Переустановить
		elif n==8:
			n=3

		if wid.name == "eventbox_wine":
			self.label_wine_n = n
			self.label_wine.set_markup(self.all_consist[self.label_wine_n])
		elif wid.name == "eventbox_hasp":
			self.label_hasp_n = n
			self.label_hasp.set_markup(self.all_consist[self.label_hasp_n])
		elif wid.name == "eventbox_font":
			self.label_font_n = n
			self.label_font.set_markup(self.all_consist[self.label_font_n])

#___________________________________________________________________________________________
# Подсветка блоков с названием пакетов
	def mouse_on_box(self, wid, param=None):
		wid.modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse("#F2F2F2"))
		self.mouse_on(wid, param)

	def mouse_off_box(self, wid, param=None):
		wid.modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse("#E6E6E6"))
		self.mouse_off(wid, param)

#___________________________________________________________________________________________
# Система подсказок
	def mouse_on(self, wid, param=None):
		if self.help_time!=None:
			gobject.source_remove(self.help_time)
		self.help_time = gobject.timeout_add(self.TIME_FOR_HELP, self.show_help, wid.name)
	
	def mouse_off(self, wid, param=None):
		if self.help_time!=None:
			gobject.source_remove(self.help_time)
		self.help_time = gobject.timeout_add(self.TIME_FOR_DOC, self.show_help, "DOC")

	def show_help(self, name):
		self.help_time=None
		self.buf_help.set_text(self._(wineGUI_help_text.text_win_install[name]))
		return False
	

#___________________________________________________________________________________________
# К оформлению
	def set_color(self, bg_color):
		# линии
		self.xml.get_widget("line_up_black").modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse("black"))
		self.xml.get_widget("line_down_black").modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse("black"))
		self.xml.get_widget("line_left_black").modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse("black"))
		self.xml.get_widget("line_right_black").modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse("black"))
		self.xml.get_widget("line_up_color").modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse(bg_color))
		self.xml.get_widget("line_down_color").modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse(bg_color))
		self.xml.get_widget("line_left_color").modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse(bg_color))
		self.xml.get_widget("line_right_color").modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse(bg_color))
		# Главный фон
		self.xml.get_widget("color_bg").modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse(bg_color))

#___________________________________________________________________________________________
# Для нормальной прорисовки	
	def GTK_update(self):
		gobject.timeout_add(200, self.wait_events)
		gtk.main()
		
	def wait_events(self):
		if gtk.events_pending()==0:
			gtk.main_quit()
			return False
		return True





