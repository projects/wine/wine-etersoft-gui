#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gtk, gtk.glade
import os, gobject
import wineGUI_functions_pkg
import grp, pwd
import popen2
import codecs
import wineGUI_win_show_log as Show_log
import wineGUI_help_text

# Окно управления wine-окружением
class win_wine_env:
	
	def __init__(self, config):
		self.config = config
		self._ = self.config.translate
		
		self.xml = gtk.glade.XML(config.get("glade_path")+"win_wine_env.glade")
		self.xml.signal_autoconnect(self)
		self.win_main = self.xml.get_widget("win_wine_env")

		# Получаем доступ к кнопкам
		self.but_create_local = self.xml.get_widget("but_create_local")
		self.but_update = self.xml.get_widget("but_update")
		self.but_create_public = self.xml.get_widget("but_create_public")
		self.but_connect = self.xml.get_widget("but_connect")
		self.but_remove = self.xml.get_widget("but_remove")
		self.but_remove_public = self.xml.get_widget("but_remove_public")
		self.but_disconnect = self.xml.get_widget("but_disconnect")
		
		# Доступ к надписям
		self.label_wine = self.xml.get_widget("label_wine")
		self.label_user = self.xml.get_widget("label_user")
		self.label_type_env = self.xml.get_widget("label_type_env")
		self.label_public_env = self.xml.get_widget("label_public_env")
		
		# Перекодировщик
		ret = codecs.lookup(self.config.get("code"))
		self.decoder = ret[1]
		
		# Окно подсказок
		self.buf_help = self.xml.get_widget("textview_help").get_buffer()
		self.help_time=None
		self.TIME_FOR_HELP = 500
		self.TIME_FOR_DOC = 5000

		# Переменные состояния
		self.wine_consist = None
		self.user_admin = None
		self.type_env = None
		self.public = None
		self.etersoft_env = None

		# Тестируем систему
		self.test_system()
		
		text = wineGUI_help_text.text_win_env["DOC"]
		self.buf_help.set_text(self._(text))
		
		self.win_main.show()

	def run(self):
		gtk.main()

	def on_but_exit(self, wid, param=None):
		self.win_main.destroy()
		gtk.main_quit()
#___________________________________________________________________________________________
# Функция, определяющая какие кнопки показывать
	def test_system(self):
		# Является ли текущий юзер - wine-admin
		user = os.getenv('USER')
		group = grp.getgrnam("wineadmin")[3]
		self.user_admin = (user in group)
		# Проверяем, существует ли каталог ~/.wine
		home_dir = pwd.getpwnam(user)[5]
		wine_path = os.access(home_dir + "/.wine/", os.F_OK)
		# Установлен ли wine
		self.wine_consist = wineGUI_functions_pkg.is_wine_install()
		# Определяем тип wine-окружения: True = Общее, False = локальное или отсутствует
		self.type_env = os.path.islink(home_dir + "/.wine/dosdevices/c:")
		# Определяем версию wine-окружения: 0 - не наша или отсутствует, 1 - наша, но старая, 2 - наша и все ок
		try:
			f = open(home_dir + "/.wine/.etersoft-release")
			self.etersoft_env = 1
			for line in f.readlines():
				if "WINE@Etersoft" in line:
					if self.wine_consist[0]>0:
						if self.wine_consist[1] in line and self.wine_consist[2] in line:
							self.etersoft_env = 2
		except:
			self.etersoft_env = 0
		
		# Существует ли общее wine-окружение
		try:
			self.public = (os.listdir("/var/lib/wine/default/")!=[])
		except:
			self.public = False
		
		# Меняем состав кнопок и надписи
		if self.wine_consist[0]>0:
			self.label_wine.set_markup("<b><span foreground='black'>Wine@Etersoft " + self.wine_consist[2] + " v." + self.wine_consist[1] + " " + self._("installed") + "</span></b>")
		else:
			self.label_wine.set_markup("<b><span foreground='blue'>" + self._("Wine@Etersoft not installed") + "</span></b>")
	
		if self.public:
			self.label_public_env.set_markup(self._("Public environment exist"))
		else:
			self.label_public_env.set_markup(self._("Public environment not exist"))
	
		if self.user_admin:
			self.label_user.set_markup(self._("You are wine-administrator"))
		else:
			self.label_user.set_markup(self._("You are Not wine-administrator"))
		
		# Кнопка Удалить общее wine-окружение
		if self.public and self.user_admin and (not self.type_env):
			self.but_remove_public.show()
		else:
			self.but_remove_public.hide()
		
		if wine_path:
			if self.etersoft_env==0:
				self.label_type_env.set_markup("<span foreground='red'>" + self._("Your wine environment exist, but incompatibility!") + "</span>")
				self.but_disconnect.hide()
				self.but_remove.show()
			elif self.etersoft_env==1:
				self.but_disconnect.hide()
				self.but_remove.show()
				self.label_type_env.set_markup("<span foreground='red'>" + self._("Your wine environment need to update!") + "</span>")
			else:
				if self.type_env:
					self.label_type_env.set_markup(self._("Your wine environment is public"))
					self.but_disconnect.show()
					self.but_remove.hide()
				else:
					self.label_type_env.set_markup(self._("Your wine environment is local"))
					self.but_disconnect.hide()
					self.but_remove.show()
			self.but_create_local.hide()
			self.but_update.show()
			self.but_create_public.hide()
			self.but_connect.hide()
		else:
			self.label_type_env.set_markup("<span foreground='red'>" + self._("Wine environment not created") + "</span>")
			if self.user_admin:
				if self.public:
					self.but_connect.show()
					self.but_create_public.hide()
				else:
					self.but_connect.hide()
					self.but_create_public.show()
				self.but_create_local.show()
				self.but_update.hide()
				self.but_remove.hide()
				self.but_disconnect.hide()
			else:
				self.but_create_local.show()
				self.but_update.hide()
				self.but_create_public.hide()
				self.but_connect.show()
				self.but_remove.hide()
				self.but_disconnect.hide()

#___________________________________________________________________________________________
# Обработчики кнопок

	# Создание локального wine-окружения
	def local_create(self, but):
		# а установлен ли наш wine
		if self.wine_consist[0]<=0:
			string = "<b><span foreground='red'>" + self._("Error:") + "\n</span></b>"
			string = string + self._("Wine@Etersoft Not Installed, can not create wine environment")
			win = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, "")
			win.set_markup(string)
			win.run()
			win.destroy()
			return

		self.win_main.hide()
		win_proc = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_NONE, self._("Create..."))
		win_proc.show_all()
		self.GTK_update()
		com = popen2.Popen4("wine")
		ret = com.wait()
		if ret!=0:
			# Удаляем созданный .wine
			com_rem = popen2.Popen3("rm -r -f " + pwd.getpwnam(os.getenv('USER'))[5] + "/.wine/", True)
			com_rem.wait()
			# Выводим сообщение
			string = "<b><span foreground='red'>" + self._("Error: Can not create wine environment") + "\n</span></b>"
			log = "Command: create local wine-environment (wine)\n"
			log = log + "Error: Can not create wine environment\n"
			log = log + "Detailes:\n\n"
			for line in com.fromchild.readlines():
				log = log + self.decoder(line)[0]
			win_proc.destroy()
			self.GTK_update()
			Show_log.show_error(self.config, gtk.MESSAGE_ERROR, string, log)
			
		else:
			string = "<b><span foreground='blue'>" + self._("Wine environment create successfully!") + "</span></b>\n"
			log = "Command: create local wine-environment (wine)\n"
			log = log + "Detailes:\n\n"
			for line in com.fromchild.readlines():
				log = log + self.decoder(line)[0]
			win_proc.destroy()
			self.GTK_update()
			Show_log.show_error(self.config, gtk.MESSAGE_INFO, string, log)
		self.test_system()
		self.win_main.show()

	# Создание общего wine-окружения
	def public_create(self, but):
		# а установлен ли наш wine
		if self.wine_consist[0]<=0:
			string = "<b><span foreground='red'>" + self._("Error") + ":\n</span></b>"
			string = string + self._("Wine@Etersoft Not Installed, can not create wine environment")
			win = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, "")
			win.set_markup(string)
			win.run()
			win.destroy()
			return

		self.win_main.hide()
		win_proc = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_NONE, self._("Create..."))
		win_proc.show_all()
		self.GTK_update()
		com = popen2.Popen4("wine --admin")
		ret = com.wait()
		if ret!=0:
			# Удаляем созданный .wine
			com_rem = popen2.Popen3("rm -r -f " + pwd.getpwnam(os.getenv('USER'))[5] + "/.wine/", True)
			com_rem.wait()
			# Выводим сообщение
			string = "<b><span foreground='red'>" + self._("Error: Can not create wine environment") + "</span></b>"
			log = "Command: create public wine-environment (wine)\n"
			log = log + "Error: Can not create wine environment\n"
			log = log + "Detailes:\n\n"
			for line in com.fromchild.readlines():
				log = log + self.decoder(line)[0]
			win_proc.destroy()
			self.GTK_update()
			Show_log.show_error(self.config, gtk.MESSAGE_ERROR, string, log)
		else:
			string = "<b><span foreground='blue'>" + self._("Wine environment create successfully!") + "</span></b>"
			log = "Command: create public wine-environment (wine)\n"
			log = log + "Detailes:\n\n"
			for line in com.fromchild.readlines():
				log = log + self.decoder(line)[0]
			win_proc.destroy()
			self.GTK_update()
			Show_log.show_error(self.config, gtk.MESSAGE_INFO, string, log)

		self.test_system()
		self.win_main.show()
		
		
	# Обновление wine-окружения
	def update(self, but):
		# а установлен ли наш wine
		if self.wine_consist[0]<=0:
			string = "<b><span foreground='red'>" + self._("Error") + ":\n</span></b>"
			string = string + self._("Wine@Etersoft Not Installed, can not connect to Public wine environment")
			win = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, "")
			win.set_markup(string)
			win.run()
			win.destroy()
			return

		# Совместимо ли wine-окружение
		if self.etersoft_env==0:
			string = "<b><span foreground='red'>" + self._("Error") + ":\n</span></b>"
			string = string + self._("Current wine environment is incompatibility, please, remove it and then create New")
			win = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, "")
			win.set_markup(string)
			win.run()
			win.destroy()
			return

		# Запрещаем простому юзеру обновлять общее wine-окружение
		if (not(self.user_admin)) and self.type_env:
			string = "<b><span foreground='red'>" + self._("Error") + ":\n</span></b>"
			string = string + self._("You are connected to Public wine environment, you can not Update this")
			win = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, "")
			win.set_markup(string)
			win.run()
			win.destroy()
			return

		# Делаем предупреждение, если мы - wine-admin и подключены к общему wine-окружению
		if self.user_admin and self.type_env:
			string = "<b><span foreground='red'>" + self._("Warning!") + "\n</span></b>"
			string = string + self._("Will be updateted Public wine environment!") + "\n"
			string = string + self._("Do you want to do it?")
			win = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_QUESTION, gtk.BUTTONS_YES_NO, "")
			win.set_markup(string)
			ret = win.run()
			win.destroy()
			if ret!=-8:
				return
		
		self.win_main.hide()
		win_proc = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_NONE, self._("Update..."))
		win_proc.show_all()
		self.GTK_update()
		com = popen2.Popen4("wine --update")
		ret = com.wait()
		if ret!=0:
			string = "<b><span foreground='red'>" + self._("Error: Can not update wine environment") + "</span></b>"
			if self.user_admin and self.type_env:
				log = "Command: update public wine-environment (wine)\n"
			else:
				log = "Command: update local wine-environment (wine)\n"
			log = log + "Error: Can not update wine environment\n"
			log = log + "Detailes:\n\n"
			for line in com.fromchild.readlines():
				log = log + self.decoder(line)[0]
			win_proc.destroy()
			self.GTK_update()
			Show_log.show_error(self.config, gtk.MESSAGE_ERROR, string, log)
		else:
			string = "<b><span foreground='blue'>" + self._("Wine environment update successfully!") + "</span></b>"
			if self.user_admin and self.type_env:
				log = "Command: update public wine-environment (wine)\n"
			else:
				log = "Command: update local wine-environment (wine)\n"
			log = log + "Detailes:\n\n"
			for line in com.fromchild.readlines():
				log = log + self.decoder(line)[0]
			win_proc.destroy()
			self.GTK_update()
			Show_log.show_error(self.config, gtk.MESSAGE_INFO, string, log)

		self.test_system()
		self.win_main.show()

	# подключение к общему wine-окружению
	def connect(self, but):
		# а установлен ли наш wine
		if self.wine_consist[0]<=0:
			string = "<b><span foreground='red'>" + self._("Error") + ":\n</span></b>"
			string = string + self._("Wine@Etersoft Not Installed, can not connect to Public wine environment")
			win = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, "")
			win.set_markup(string)
			win.run()
			win.destroy()
			return
		
		# Существует ли общее окружение
		if not self.public:
			string = "<b><span foreground='red'>" + self._("Error") + ":\n</span></b>"
			string = string + self._("Public wine environment don't exist! Can not connect.\nOnly Wine-Administrator can create Public wine environment")
			win = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, "")
			win.set_markup(string)
			win.run()
			win.destroy()
			return

		self.win_main.hide()
		win_proc = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_NONE, self._("Connect..."))
		win_proc.show_all()
		self.GTK_update()
		com = popen2.Popen4("wine --attach")
		ret = com.wait()
		if ret!=0:
			# Удаляем созданный .wine
			com_rem = popen2.Popen3("rm -r -f " + pwd.getpwnam(os.getenv('USER'))[5] + "/.wine/", True)
			com_rem.wait()
			# Выводим сообщение
			string = "<b><span foreground='red'>" + self._("Error: Can not connect to Public wine environment") + "</span></b>"
			log = "Command: connect to public wine-environment (wine)\n"
			log = log + "Error: Can not connect to wine environment\n"
			log = log + "Detailes:\n\n"
			for line in com.fromchild.readlines():
				log = log + self.decoder(line)[0]
			win_proc.destroy()
			self.GTK_update()
			Show_log.show_error(self.config, gtk.MESSAGE_ERROR, string, log)
		else:
			string = "<b><span foreground='blue'>" + self._("Successfully connected to the Wine environment!") + "</span></b>"
			log = "Command: connect to public wine-environment (wine)\n"
			log = log + "Detailes:\n\n"
			for line in com.fromchild.readlines():
				log = log + self.decoder(line)[0]
			win_proc.destroy()
			self.GTK_update()
			Show_log.show_error(self.config, gtk.MESSAGE_ERROR, string, log)

		self.test_system()
		self.win_main.show()

		
	# Удаление .wine
	def remove(self, but):
		# Предупреждение для администратора, подключенного к общему wine-окружению
		if self.user_admin and self.type_env:
			string = "<b><span foreground='red'>" + self._("Warning!") + "\n</span></b>"
			string = string + self._("Will be Disconnected from PUBLIC wine environment!") + "\n"
			string = string + self._("For Remove PUBLIC wine environment use Clean Public button!") + "\n"
			string = string + self._("Continue?")
			win = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_QUESTION, gtk.BUTTONS_YES_NO, "")
			win.set_markup(string)
			ret = win.run()
			win.destroy()
			if ret!=-8:
				return
			
		# Предупреждение для простого юзера, подключенного к общему wine-окружению
		if (not(self.user_admin)) and self.type_env:
			string = "<b><span foreground='red'>" + self._("Warning!") + "\n</span></b>"
			string = string + self._("Will be Disconnected from PUBLIC wine environment!") + "\n"
			string = string + self._("Do you want to do it?")
			win = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_QUESTION, gtk.BUTTONS_YES_NO, "")
			win.set_markup(string)
			ret = win.run()
			win.destroy()
			if ret!=-8:
				return

		# Делаем предупреждение, что ща все убьем
		string = "<b><span foreground='red'>" + self._("Warning!") + "\n</span></b>"
		string = string + self._("Path '~/.wine' will be removed with all including PATH and FILES!!!") + "\n"
		string = string + self._("Do you want to remove it?")
		win = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_QUESTION, gtk.BUTTONS_YES_NO, "")
		win.set_markup(string)
		ret = win.run()
		win.destroy()
		if ret!=-8:
			return
		
		self.win_main.hide()
		win_proc = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_NONE, self._("Remove..."))
		win_proc.show_all()
		self.GTK_update()
		user = os.getenv('USER')
		home_dir = pwd.getpwnam(user)[5]
		com = popen2.Popen3("rm -r -f " + home_dir + "/.wine/", True)
		ret = com.wait()
		if ret!=0:
			if but.name=="but_remove":
				string = "<b><span foreground='red'>" + self._("Error: Can not Remove wine environment") + "</span></b>"
				log = "Command: remove local wine-environment (wine)\n"
				log = log + "Error: Can not remove wine environment\n"
			else:
				string = "<b><span foreground='red'>" + self._("Error: Can not Disconnect from Public wine environment") + "</span></b>"
				log = "Command: disconnect from public wine-environment (wine)\n"
				log = log + "Error: Can disconnect from public wine environment\n"
			log = log + "Detailes:\n\n"
			for line in com.childerr.readlines():
				log = log + self.decoder(line)[0]
			win_proc.destroy()
			self.GTK_update()
			Show_log.show_error(self.config, gtk.MESSAGE_ERROR, string, log)
		else:
			if but.name=="but_remove":
				string = "<b><span foreground='blue'>" + self._("Successfully removed Wine environment!") + "</span></b>\n"
			else:
				string = "<b><span foreground='blue'>" + self._("Successfully disconnected from Public Wine environment!") + "</span></b>\n"
			win = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_OK, "")
			win.set_markup(string)
			win_proc.destroy()
			self.GTK_update()
			win.run()
			win.destroy()
		
		self.test_system()
		self.win_main.show()

	# Удаление общего wine-окружения
	def remove_public(self, but):
		# Делаем предупреждение, что ща все убьем
		string = "<b><span foreground='red'>" + self._("Warning!") + "\n</span></b>"
		string = string + self._("All path in /var/lib/wine/default/ will be removed!") + "\n"
		string = string + self._("Do you want to remove it?")
		win = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_QUESTION, gtk.BUTTONS_YES_NO, "")
		win.set_markup(string)
		ret = win.run()
		win.destroy()
		if ret!=-8:
			return
		
		self.win_main.hide()
		win_proc = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_NONE, self._("Remove..."))
		win_proc.show_all()
		self.GTK_update()
		list_dir = os.listdir("/var/lib/wine/default")
		for item in list_dir:
			com = popen2.Popen3("rm -r -f '/var/lib/wine/default/" + item + "'", True)
			ret = com.wait()
			if ret!=0:
				string = "<b><span foreground='red'>" + self._("Error: Can not Remove Public wine environment") + "</span></b>"
				log = "Command: remove public wine-environment (wine)\n"
				log = log + "Error: Can not remove wine environment\n"
				log = log + "Detailes:\n\n"
				for line in com.childerr.readlines():
					log = log + self.decoder(line)[0]
				win_proc.destroy()
				self.GTK_update()
				Show_log.show_error(self.config, gtk.MESSAGE_ERROR, string, log)
				self.test_system()
				self.win_main.show()
				return
			
		string = "<b><span foreground='blue'>" + self._("Successfully removed Public Wine environment!") + "</span></b>\n"
		win = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_OK, "")
		win.set_markup(string)
		win_proc.destroy()
		self.GTK_update()
		win.run()
		win.destroy()


		self.test_system()
		self.win_main.show()



#___________________________________________________________________________________________
# Система подсказок
	def mouse_on(self, wid, param=None):
		if self.help_time!=None:
			gobject.source_remove(self.help_time)
		self.help_time = gobject.timeout_add(self.TIME_FOR_HELP, self.show_help, wid.name)
	
	def mouse_off(self, wid, param=None):
		if self.help_time!=None:
			gobject.source_remove(self.help_time)
		self.help_time = gobject.timeout_add(self.TIME_FOR_DOC, self.show_help, "DOC")

	def show_help(self, name):
		self.help_time=None
		self.buf_help.set_text(self._(wineGUI_help_text.text_win_env[name]))
		return False


#___________________________________________________________________________________________
# Для нормальной прорисовки	
	def GTK_update(self):
		gobject.timeout_add(200, self.wait_events)
		gtk.main()
		
	def wait_events(self):
		if gtk.events_pending()==0:
			gtk.main_quit()
			return False
		return True
