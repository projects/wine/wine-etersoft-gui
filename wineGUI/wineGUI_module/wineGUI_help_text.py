#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gettext

# В этом файле хранятся все тексты помощи

# Только для автоматического добавления в файл переводов
_ = gettext.NullTranslations().ugettext

# Главное окно
text_win_main = {
"DOC":_("This program can support you to manage Wine@Etersoft\nSelect button for some action"),
"button_install":_("Section for Install, Uninstall and Update WINE and other components"),
"button_wine_env":_("Management of Public and Local Wine-Environment: create, update and remove"),
"button_config":_("Section for configuration Wine: Local and Public Wine-Resources, Administrate Install"),
"button_diag":_("Diagnostics Wine and Win-programs"),
"but_win_prog":_("Help for use Wine@Etersoft and install win-programs"),
"but_establish":_("Check authenticity of Wine@Etersoft - visit your page on our site."),
"button_exit":_("Exit from progam")
}


# Окно управления пакетами
text_win_install = {
"DOC":_("On this window you can Install, Update, Reinstall or Remove Programs. For select Action of program press on the program name. Options 'Installed' and 'Not install' mean Not action with program. On the top piece of window you can select your Linux distribution, if it does not found automaticaly. For execute selected action press Next."),
"but_next":_("Next step: execute selected action.\nWarning: check, that Linux distribution indicate correctly"),
"but_back":_("Return to previos window. All selected action will be canceled"),
"eventbox_wine":_("Program Wine@Etersoft. If Wine@Etersoft not installed, you can only Install packages, at that, all old wine packages will be removed. If installed some version of Wine@Etersoft, you can Update our product (on new version), Reinstall program (if Update is impossibly), or Remove product with all Wine@Etersoft pakcages"),
"eventbox_hasp":_("Driver HASP 4/HL Aladdin company: this driver needs for work with port protection device (local and network). Install this driver only if port protection device set in your PC."),
"eventbox_font":_("MS Core Fonts. This fonts need for correctly work of win-programs. You can try Install this fonts, Reinstall or Remove. If this actions impossibly, it to be no avail"),
"checkbutton":_("Often, for Install Linux programs needs other subsidiary packages. This option allow automatically satisfy dependences. Warning: if this option set, installation will be run in individual console.")
}


# Окно управления wine-окружением
text_win_env = {
"DOC":_("For use Wine needs Wine Environment. Win-programs install in this Environment. In this window you can select your Wine Environment"),
"but_create_local":_("Create local Wine Environment. In your home path will be create '.wine' directory with Wine Environment"),
"but_update":_("Update Wine Environment (Local or Public). This function recommended after Update Wine@Etersoft"),
"but_create_public":_("Create Public Wine Environment. Then, all users can connect to this Environment and use win-program, installed into it"),
"but_connect":_("Connect to Public Wine Environment. After connect, you can use win-program, installed into this Environment"),
"but_remove":_("Remove Wine Environment. After removing, you can create New Wine Environment or connect to Public Environment"),
"but_disconnect":_("Disconnect Wine Environment. After disconnect, you can create New Wine Environment or connect to Public again"),
"but_remove_public":_("Remove Public Wine Environment. Will be removed all path in '/var/lib/wine/default/'"),
"but_exit":_("Return to main window")
}


# Окно конфигурации wine
text_win_conf = {
"DOC":_("In this window you can configure Wine and Wine Environment. To global configuration needs root rights"),
"but_wineadmin":_("Help for configure Administrate Install and selection Wine-administrator"),
"but_pulic_res":_("Configure Default Public resources for Wine Environment: add to default win-disks and network path for all users"),
"but_winecfg":_("Start standart wine utility - winecfg. It support configurations your local Wine Environment"),
"but_exit":_("Return to main window")
}
# Диалог подтверждения запуска редактирования дисков
text_win_conf_edit_disk = _("In this part you can create Public win-Disk or Network Path for all users\nWarning! This resources will be created only with creating users wine Environment!")


# Диалоговое окно конфигурации Административной установки
text_wineadmin_1 = _("\tFor Administration Install need Wine-Administrator. This user will be install win-programs in Public Wine Environment for all other users.\n\tWine-Administrator is user, which enter in 'wineadmin' group. In theory, you can make several wine-administrators, but recomended only one.\n")
# Тут в окне идет надпись о количестве созданных юзеров и далее этот текст
text_wineadmin_2 = _("\tAfter wine-administrator selected, create on his behalf Public Wine Environment, and install programs to it (on disk C: from wine-administartor).\n")
# Конечное окно
text_wineadmin_3 = _("Warning! If new wine-administrator were login, changes will come into effect only after full relogin.\n")
text_wineadmin_4 = _("Now, you can create Public Wine Environment, or Connect to it (if it were created), and Install win-programs.\nOther users can connect to Public Wine Environment for use win-programs, installed to it")
# Текст, передаваемый в окно редактирования групп
text_wineadmin_5 = _("In this window you can change group wineadmin for create or remove Wine Administrator. Recomeded select only one wine-admin")



# Окно редактирования групп
text_edit_group = {
"but_add":_("Add user to Group. The same way as, You can make double click by user"),
"but_remove":_("Remove user from Group. The same way as, You can make double click by user"),
"treeview_all":_("List of all user in system, except users in Edit Group"),
"treeview_group":_("List users of Edit group"),
"but_cancel":_("Return to previos window and Canceled all overpatching"),
"but_ok":_("Accept all overpatching and go to the next step")
}

# Окно редактирования ресурсов
text_edit_disk = {
"DOC":_("In this window you can edit list of default public resources. This resources will be created for all users with creating Wine Environment"),
"treeview":_("List resources, which already created. You can add new resource and then Modyfy and Remove It."),
"entry_name_res":_("Field for name Resource. If type of resource is Disk, then will be created standart win-disk in Wine Environment. If type is Network, then will be created network path (as \\\\server\\path\\dir)"),
"entry_real_adress":_("Field for real adress in Linux file system, which will be used for resource. This adress must be exist"),
"but_select_dir":_("Button for select directory for real adress of resource"),
"but_edd_res":_("Add new resource. Warning! This resource will be connected to user only with creating new users Wine Environment"),
"but_update_res":_("Modyfy resource. Warning! This resource will be update at user only with creating new users Wine Environment"),
"but_remove_res":_("Remove resource. Warning! This resource will be remove at user only with creating new users Wine Environment"),
"but_exit":_("Return to previous window")
}

# Окно диагностики
text_win_diag = {
"DOC":_("Select type diagnostic. After execution this utilites, you will received Log, which can send to our Support"),
"but_windiag":_("Run utility winediag for testing wine system files"),
"but_winelog":_("Run utility winelog for testing run win-programs"),
"but_additional":_("Section with different additional diagnostics for your system"),
"but_exit":_("Return to main window")
}
text_win_diag_run_1 = _("This utility allow create log of run win-programs. This information needed for debugging work win-programs. Log file is accumulative, it dispose in the path")
text_win_diag_run_2 = _("If file is very large, you can remove it by hand\nNow, press OK, and select win-program for create log, or Cancel for Exit")


# Скрипт установки
text_script_admin = _("Administrate Install mode allow Install win-programs for several users at once")
text_script_disk = _("In this part you can create Public win-Disk or Network Path for all users\nWarning! This resources will be created only with creating users wine Environment!")
text_script_finish = _("Now, for use Wine@Etersoft, you must create wine Environment in the main window\n")
text_script_update = _("Now, recomended update wine Environment\nUse button in main window")


