#!/usr/bin/env python
# -*- coding: utf-8 -*-

import popen2

# Функция определения дистрибутива
# Если дистрибутив не удалось определить - возвращает ["",""]
# Если версию не удалось определить - возвращает ["Имя",""]
def get_distrib(mod_dir):
	# Определяем дистрибутив
	com = popen2.Popen3(mod_dir + "/distr_vendor -d", True)
	ret = com.wait()
	if ret!=0:
		return ["", ""]
	distrib = com.fromchild.readline()
	distrib = distrib.replace("\n", "")
	distrib = distrib.strip()
	if not(distrib in all_distrib.keys()):
		distrib = ""
		return ["", ""]
	# Определяем версию
	com = popen2.Popen3(mod_dir + "distr_vendor -v", True)
	ret = com.wait()
	if ret!=0:
		return [distrib, ""]
	version = com.fromchild.readline()
	version = version.replace("\n", "")
	version = version.strip()
	if not(version in all_distrib[distrib]):
		version = ""
	return [distrib, version]
	

# Функция, определяющая, какой тип пакетов используется в системе
def get_type_pkg(distrib, mod_dir):
	output = popen2.popen3("distr_vendor -p " + distrib)
	if output[2].readline()!="":
		return ""
	pkg = output[0].readline()
	pkg = pkg.replace("\n", "")
	pkg = pkg.strip()
	if not(pkg in ["rpm", "pkg", "tgz", "deb", "tbz2"]):
		return ""
	return pkg

# Список всех поддерживаемых дистрибутивов
all_distrib = {
'ALTLinux': ['2.3', '2.4', '3.0', 'Sisyphus'],
'ASPLinux': ['10', '11', '11.2'],
'CentOS': ['4.0'],
'Debian': ['3.1','4.0'],
'FedoraCore': ['3', '4', '5', '6'],
'FreeBSD': ['5.4', '6.0', '6.1', ],
'Knoppix': ['4.0'],
'LinuxXP': ['2006'],
'MOPSLinux': ['4.0', '4.1', '5.0'],
'Mandriva': ['2005', '2006', '2007'],
'NLD': ['9'],
'RHEL': ['3', '4'],
'RedHat': ['9'],
'SLED': ['10', '9'],
'Scientific': ['4.1'],
'Slackware': ['10.2', '11'],
'SUSE': ['10', '10.1', '10.2'],
'Ubuntu': ['5.10', '6.06', '6.10'],
'Gentoo': ['2006.1']
}





