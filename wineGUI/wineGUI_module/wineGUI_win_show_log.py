#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gtk, gtk.glade
import os
import urllib
import wineGUI_distrib
import wineGUI_functions_pkg

# Функция вызова класса
def show_error(config, mode, err_string, log, head="<b>Log of operation</b>"):
	_ = config.translate
	win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, mode, gtk.BUTTONS_OK, "")
	# Создаем кнопку лога
	but_log = gtk.Button()
	box = gtk.HBox()
	but_log.add(box)
	but_log.set_relief(gtk.RELIEF_NORMAL)
	#img = gtk.Image()
	#img.set_from_stock(gtk.STOCK_INFO, gtk.ICON_SIZE_BUTTON)
	label = gtk.Label(_("Show log"))
	#box.pack_start(img)
	box.pack_end(label)
	
	win.add_action_widget(but_log, 1)
	win.set_markup(err_string)
	win.show_all()
	ret = win.run()
	win.destroy()
	# Если нажали лог
	if ret==1:
		win = win_show_log(config, log, head)
		win.run()


# Класс окна отображения лога операции
class win_show_log:
	
	def __init__(self, config, log, head=None):
		self.config = config
		self._ = self.config.translate
		if head==None:
			head="<b>" + self._("Log of operation") + "</b>"
		
		self.xml = gtk.glade.XML(config.get("glade_path")+"win_show_log.glade")
		self.xml.signal_autoconnect(self)
		self.win_main = self.xml.get_widget("win_show_log")
		
		# Заголовок окна
		self.xml.get_widget("label_head").set_markup(head)
		
		# Окно отображения лога
		self.buf_log = self.xml.get_widget("text_log").get_buffer()
		self.text = log
		# Добавляем информацию о системе
		self.text = self.text + "\n\n\nInformation about System:\n"
		self.text = self.text + "Linux distribution: "
		distr = wineGUI_distrib.get_distrib(self.config.get("module_path"))
		if distr[0]!='':
			self.text = self.text + distr[0]
			if distr[1]!='':
				self.text = self.text + "  " + distr[1] + "\n"
		else:
			self.text = self.text + config.get("linux_distrib") + "  " + config.get("linux_version") + "\n"
		self.text = self.text + "Wine product (installed): "
		wine_consist = wineGUI_functions_pkg.is_wine_install()
		if wine_consist[0]>0:
			self.text = self.text + "Wine@Etersoft " + wine_consist[2] + " v." + wine_consist[1] + "\n"
		else:
			self.text = self.text + "Not install\n"
		self.text = self.text + "Wine product (distribution): " + config.get("wine_name") + " v." + config.get("gui_version") + "\n"
		try:
			self.text = self.text + "Serial number (installed): " + wine_consist[3] + "\n"
		except:
			pass
		self.text = self.text + "Serial number (distribution): " + config.get("serial") + "\n"
		#self.text = self.text + ": " +  + "\n"
		#self.text = self.text + ": " +  + "\n"
		#self.text = self.text + ": " +  + "\n"
		#self.text = self.text + ": " +  + "\n"
		
		# Выводим лог на экран
		self.buf_log.set_text(self.text)

		# Для сохранения в файл
		self.selected = ""

		self.win_main.show_all()

	def run(self):
		gtk.main()

#___________________________________________________________________________________________
# Обработка нажатия кнопок
	
	def on_but_exit(self, wid, param=None):
		self.win_main.destroy()
		gtk.main_quit()

	# Копирование текста
	def on_but_copy(self, but):
		self.buf_log.select_range(self.buf_log.get_start_iter(), self.buf_log.get_end_iter())
		self.buf_log.copy_clipboard(gtk.Clipboard())
		
	# Сохранение текста
	def on_but_save(self, but):
		win = gtk.FileChooserDialog(but.name, self.win_main, gtk.FILE_CHOOSER_ACTION_SAVE, (gtk.STOCK_CANCEL, gtk.RESPONSE_REJECT, gtk.STOCK_OK, gtk.RESPONSE_ACCEPT))
		win.connect("response", self.save_file )
		win.run()
		if self.selected!="":
			if os.access(self.selected, os.F_OK):
				text = self._("Warning: file") + " " + self.selected + " " + self._("already exist! Do you want rewrite it?")
				win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_QUESTION, gtk.BUTTONS_YES_NO, text)
				ret = win.run()
				win.destroy()
				if ret != -8:
					return
			try:
				file = open(self.selected, "w")
				file.write(self.text)
				file.close()
				text = self._("Save is OK")
				win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_OK, text)
				win.run()
				win.destroy()
			except:
				text = self._("Error: can not save log to file") + " " + self.selected
				win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, text)
				win.run()
				win.destroy()
		
	def save_file(self, dialog, resp_id):
		if resp_id == gtk.RESPONSE_REJECT or resp_id==-4:
			dialog.destroy()
			self.selected=""
			return
		if resp_id == gtk.RESPONSE_ACCEPT:
			self.selected = dialog.get_filename()
			dialog.destroy()
			
	def on_but_send(self, but):
		text = self._("Are you sure, that you want send this log to support?")
		win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_QUESTION, gtk.BUTTONS_YES_NO, text)
		ret = win.run()
		win.destroy()
		if ret != -8:
			return
		try:
			urllib.urlopen("url", "param")
			text = self._("Sand is OK")
			win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_OK, text)
			win.run()
			win.destroy()

		except:
			text = self._("Error: can not connect to support")
			win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, text)
			win.run()
			win.destroy()

		
		


