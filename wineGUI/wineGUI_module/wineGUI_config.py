#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Модуль для работы с файлом конфигурации config

class wineGUI_config:
	
	def __init__(self, file_name):
		# список параметров
		self.all = {"lang":None, "linux_distrib":None, "linux_version":None, "glade_path":None, "module_path":None, "code":None,\
		 "lang_path":None, "gui_version":None, "serial":None, "wine_name":None, "pkg_dir":None, "ver_date": None, "type_root": None,
		 "locale":None, "test_mode":None, "owner":None, "help_path":None, "test_path":None, "browser":None, "type_install":None,
		 "type_console":""}

		self.comment = {"lang":"Lang", "linux_distrib":"Linux distrib", "linux_version":"Linux distrib version", \
		"glade_path":"Path for Glade elements", "module_path":"Path for python modules","lang_path":"Path for lang",\
		"gui_version":"WineGUI version", "serial":"Wine serial number", "wine_name":"Name oh wine@etersoft product",\
		"pkg_dir":"Directory with WINE Packages", "ver_date":"Date of relase", "type_root":"Type get user root",\
		"code":"Current console coding", "locale":"current locale", "test_mode":"Is test mode? True or False", "owner":"Owner name", \
		"help_path":"Help files for install win-programs", "browser":"Default browser to use (firefox, konqueror, mozilla, seamonkey, opera)", \
		"test_path":"Path with additional tests", "type_install":"Type of system instalator: apt-get or yum",
		"type_console":"default console-program (xterm, konsole)"}

		# Поле для хранения функции переводов
		self.translate = None
		
		# Открываем файл и вычитываем конфигурацию
		self.config_file = file_name
		self.read_config()

	
	def read_config(self):
		try:
			file = open(self.config_file, "r")
		# Если не удалось открыть файл, создаем его с исходной конфигурацией
		except:
			print "Can not open config file, create new with default"
			self.new_config()
			return
		lines = file.readlines()
		file.close()
		# Если файл пустой
		if len(lines)==0:
			print "Config file is clean, create new with default"
			self.new_config()
			return
		# Вычитываем параметры из файла
		for line in lines:
			if (line.lstrip()==""):
				continue
			if ((line.lstrip())[0]=="#"):
				continue
			line = line.expandtabs(1)
			#line = line.replace(" ", "")
			line = line.replace("\n", "")
			line = line.strip()
			for key in self.all.keys():
				if (key+"=") in line.replace(" ", ""):
					line = line.replace(key,"")
					line = line.lstrip()
					line = line.replace("=","")
					self.all[key] = line = line.lstrip()
		# Проверяем, все ли удалось вычитать
		if None in self.all.values():
			for key, val in self.all.items():
				if val==None:
					self.all[key]=""
			self.write_config()

	def write_config(self):
		try:
			file = open(self.config_file, "w")
			file.write("# Warning! This file generated automaticaly\n")
			for key in self.all.keys():
				try:
					string = "# " + self.comment[key] + "\n"
					file.write(string)
				except:
					# Нет комента для этой опции
					pass
				string = str(key) + " = " + self.all[key] + "\n"
				file.write(string)
			file.close()
		except:
			print "Can not write config file"


	def new_config(self):
		try:
			file = open(self.config_file, "w")
			file.write("# Warning! This file generated automaticaly\n")
			for key in self.all.keys():
				self.all[key]=""
				try:
					string = "# " + self.comment[key] + "\n"
					file.write(string)
				except:
					# Нет комента для этой опции
					pass
				string = str(key) + " = \n"
				file.write(string)
			file.close()
		except:
			print "Can not create config file"

	# Взятие опции по ее имени
	def get(self, name):
		value = None
		try:
			value = self.all[name]
		except:
			print "Option " + name + " not found"
		return value
	
	# Установка опции без сохранения
	def set(self, name, value):
		if type(value)!=type(" "):
			print "Value must be String"
			return False
		try:
			self.all[name] = value
			return True
		except:
			print "Option " + name + " not found"
		return False
	
	
	
	
	
	
	
	