#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import gtk, gtk.glade, gobject

global lang

# Возвращает каталог с файлом переводов и название локали для этого каталога
def choice_language(lang_path, gtk_path):
	global lang
	lang = None
	
	def on_ok_clicked(but_name):
		global lang
		lang = ComBox.get_active()
		gtk.main_quit()
		
		
	try:
		lang_list = os.listdir(lang_path)
	except:
		#print "Can not find path '" + lang_path + "' for select lang. Set default lang"
		return ["default", "default"]
		
	lang_dir = {}		# Словарь: ключ - название языка, значение - [каталог, имя локали]
	# Пробегаемся по каталогам и ищем языки
	for item in lang_list:
		try:
			f = open(lang_path+"/"+item+"/text", "r")
			lang_name = f.readline()
			loc_name = []
			while 1:
				tmp = f.readline()
				if tmp=="":
					break
				tmp = tmp.replace("\n", "")
				loc_name.append(tmp)
			f.close()
			if lang_name!="" and loc_name!="":
				lang_dir[lang_name] = [item, loc_name]
		except:
			pass
	
	# Добавляем стандартный язык
	if lang_dir=={}:
		return ["default", "default"]

	lang_dir["English"] = ["default", "default"]
	
	# Открываем окошко со списском языков
	xml = gtk.glade.XML(gtk_path + "/win_selector.glade")
	
	ComBox = gtk.combo_box_new_text()
	xml.get_widget("combobox_selector").pack_start(ComBox)
	
	box_lang = []
	for item in lang_dir.keys():
		ComBox.append_text(item)
		box_lang.append(item)
	ComBox.set_active(0)
		
	text = xml.get_widget("text_select")
	text.set_text("Select language")
	
	xml.signal_connect("on_ok_clicked", on_ok_clicked )
	win = xml.get_widget("win_selector")
	win.show_all()
	
	gtk.main()
	win.destroy()
	try:
		lang_txt = box_lang[lang]
		return lang_dir[lang_txt]
	except:
		return ["default", "default"]






