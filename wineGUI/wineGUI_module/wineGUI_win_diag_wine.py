#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gtk, gtk.glade
import gobject
import popen2
import pwd, os, os.path
import codecs
import wineGUI_win_show_log as Show_log
import wineGUI_functions_pkg
import wineGUI_help_text
import wineGUI_win_add_diags

# Класс окна конфигурации

class win_diag_wine:

	def __init__(self, config):
		self.config = config
		self._ = self.config.translate

		self.xml = gtk.glade.XML(config.get("glade_path")+"win_diag_wine.glade")
		self.xml.signal_autoconnect(self)
		self.win_main = self.xml.get_widget("win_diag_wine")
		
		# Отображаем установленную версию wine
		wine_consist = wineGUI_functions_pkg.is_wine_install()
		if wine_consist[0]>0:
			self.xml.get_widget("label_wine").set_markup("<b><span size='12000'>Wine@Etersoft " + wine_consist[2] + " v." + wine_consist[1] + " " + self._("Installed") + "</span></b>")
		else:
			self.xml.get_widget("label_wine").set_markup("<b><span size='12000'>Wine@Etersoft " + self._("not install") + "</span></b>")

		# Перекодировщик
		ret = codecs.lookup(self.config.get("code"))
		self.decoder = ret[1]

		# Окно подсказок
		self.buf_help = self.xml.get_widget("text_help").get_buffer()
		text = wineGUI_help_text.text_win_diag["DOC"]
		self.buf_help.set_text(self._(text))
		self.help_time=None
		self.TIME_FOR_HELP = 500
		self.TIME_FOR_DOC = 5000
		
		# Для окна выбора файлов
		self.selected = ""
		
		self.win_main.show_all()
		
		
	def run(self):
		gtk.main()

#___________________________________________________________________________________________
# Обработчики кнопок

	def on_but_exit(self, but, param=None):
		self.win_main.destroy()
		gtk.main_quit()
		

	def on_but_winediag(self, but):
		self.win_main.hide()
		win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_NONE, self._("Running winediag....."))
		win.show_all()
		self.GTK_update()
		com = popen2.Popen4("winediag")
		ret = com.wait()
		log = "Command: run winecfg\n"
		if ret!=0:
			string = "<b>" + self._("WineDiag exit with Error!") + "</b>"
			log = log + "Error: can not run, return code = " + str(ret) + "\n"
		else:
			string = "<b>" + self._("WineDiag existed succesfully!") + "</b>"
			log = log + "Error: No error\n"
		log = log + "Detailes:\n\n"
		for line in com.fromchild.readlines():
			log = log + self.decoder(line)[0]
		win.destroy()
		self.GTK_update()
		if ret!=0:
			Show_log.show_error(self.config, gtk.MESSAGE_ERROR, string, log)
		else:
			Show_log.show_error(self.config, gtk.MESSAGE_INFO, string, log)
		self.win_main.show_all()
		
	def on_but_winelog(self, but):
		self.win_main.hide()
		user = os.getenv('USER')
		home_dir = pwd.getpwnam(user)[5]
		# Выводим приглашение на выбор запускаемого файла
		string = self._(wineGUI_help_text.text_win_diag_run_1) + "\n" + home_dir + "/.wine/wine.log\n" 
		if os.access(home_dir + "/.wine/wine.log", os.F_OK) == 1:
			string = string + self._("Now, size of this file") + " " + str(os.path.getsize(home_dir + "/.wine/wine.log")) + " " + self._("Bytes.") + " "
		else:
			string = string + self._("Now this file is not created. It will be created automaticaly")

		string = string + self._(wineGUI_help_text.text_win_diag_run_2)
		
		win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_QUESTION, gtk.BUTTONS_OK_CANCEL, "")
		win.set_markup(string)
		ret = win.run()
		win.destroy()
		# Если не хотим ниче удалять, то выходим
		if ret!=-5:
			self.win_main.show_all()
			return
		# Запрашиваем путь к файлу
		win = gtk.FileChooserDialog(but.name, self.win_main, gtk.FILE_CHOOSER_ACTION_OPEN, (gtk.STOCK_CANCEL, gtk.RESPONSE_REJECT, gtk.STOCK_OK, gtk.RESPONSE_ACCEPT))
		win.set_current_folder(home_dir + "/.wine/dosdevices/c:")
		win.connect("response", self.win_file_select )
		win.run()
		if self.selected!="":
			win_com = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_NONE, self._("Running winelog....."))
			win_com.show_all()
			self.GTK_update()
			com = popen2.Popen4("winelog " + self.selected)
			ret = com.wait()
			log = "Command: run winelog\n"
			if ret!=0:
				string = "<b>" + self._("WineLog exit with Error!") + "</b>"
				log = log + "Error: can not run, return code = " + str(ret) + "\n"
				log = log + "Detailes:\n\n"
				for line in com.fromchild.readlines():
					log = log + self.decoder(line)[0]
				win_com.destroy()
				self.GTK_update()
				Show_log.show_error(self.config, gtk.MESSAGE_ERROR, string, log)
			else:
				win_com.destroy()
				self.GTK_update()
				string = "<b>" + self._("WineLog existed succesfully!") + "</b>\n"
				string = string + self._("Now you can show log file") + "\n" + home_dir + "/.wine/wine.log\n"
				string = string + "and send it to our support. Recomended previously archive this file."
				win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_OK, "")
				win.set_markup(string)
				win.run()
				win.destroy()

		self.win_main.show_all()

	def on_but_additional(self, but):
		self.win_main.hide()
		win_com = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_NONE, self._("Create test list..."))
		win_com.show_all()
		self.GTK_update()
		win = wineGUI_win_add_diags.win_add_diags(self.config)
		win_com.destroy()
		self.GTK_update()
		win.run()
		self.win_main.show_all()


	def win_file_select(self, dialog, resp_id):
		if resp_id == gtk.RESPONSE_REJECT or resp_id==-4:
			dialog.destroy()
			self.selected = ""
			return
		if resp_id == gtk.RESPONSE_ACCEPT:
			self.selected = dialog.get_filename()
			dialog.destroy()

#___________________________________________________________________________________________
# Система подсказок
	def mouse_on(self, wid, param=None):
		if self.help_time!=None:
			gobject.source_remove(self.help_time)
		self.help_time = gobject.timeout_add(self.TIME_FOR_HELP, self.show_help, wid.name)
	
	def mouse_off(self, wid, param=None):
		if self.help_time!=None:
			gobject.source_remove(self.help_time)
		self.help_time = gobject.timeout_add(self.TIME_FOR_DOC, self.show_help, "DOC")

	def show_help(self, name):
		self.help_time=None
		self.buf_help.set_text(self._(wineGUI_help_text.text_win_diag[name]))
		return False

#___________________________________________________________________________________________
# Для обновления окна

	def wait_events(self):
		if gtk.events_pending()==0:
			gtk.main_quit()
			return False
		return True
	
	def GTK_update(self):
		gobject.timeout_add(200, self.wait_events)
		gtk.main()



