#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gtk, gtk.glade
import os, popen2
import gobject
import codecs

# Окно для выбора пути до пакетов wine
class win_pkg:
	def __init__(self, config, type_do, type_distrib):
		self.config = config
		self._ = self.config.translate
		
		self.type_do = type_do
		self.depend = False
		if self.type_do > 10:
			self.type_do = self.type_do - 10
			self.depend = True
		self.type_distrib = type_distrib
		self.xml = gtk.glade.XML(config.get("glade_path")+"win_select_pkg.glade")
		self.xml.signal_autoconnect(self)
		self.win_main = self.xml.get_widget("win_pkg")
		
		self.xml.get_widget("label_distrib").set_markup("<b><span size='12000' foreground='blue'>"+config.get("linux_distrib") + " " + config.get("linux_version")+"</span></b>")

		if self.type_distrib=="wine":
			product = "Wine@Etersoft " + config.get("wine_name") + " v." + config.get("gui_version")
			self.xml.get_widget("label_entry").set_markup("<b>" + self._("Select directory with WINE packages for your Linux version") +":</b>")
		elif self.type_distrib=="hasp":
			product = "Aladdin HASP 4/HL driver"
			self.xml.get_widget("label_entry").set_markup("<b>" + self._("Select directory with HASP package for your Linux version") + ":</b>")
		elif self.type_distrib=="font":
			product = "MS Core Fonts"
			self.xml.get_widget("label_entry").set_markup("<b>" + self._("Select directory with MS Core Fonts package for your Linux version") + ":</b>")
		
		if type_do==2:
			self.xml.get_widget("label_help").set_markup(self._("For Install") + " " + product + " " + self._("need next packages:"))
		elif type_do==5:
			self.xml.get_widget("label_help").set_markup(self._("For Update program to") + " " + product + " " + self._("need next packages:"))
		elif type_do==6 or type_do==3:
			self.xml.get_widget("label_help").set_markup(self._("For Install") + " " + product + " " + self._("after Uninstall need next packages:"))
		self.ret_val = -1
		
		# Блокируем кнопку Next, пока не будут найдены все пакеты
		self.but_next = self.xml.get_widget("button_next")
		self.but_next.set_sensitive(False)
		
		# Определяем набор пакетов:
		# Файл лицензии
		self.license_file = ""
		# Словарь соответствия пакетов в конфиге
		self.PKG_config = {}
		# Список используется для сортировки (словарь возвращает ключи как попало)
		if self.type_distrib=="wine":
			self.names_pkg = ["libwine", "wine", "wine-etersoft"]
			self.PKG_config.update({"wine":"WINE_pkg"})
			self.PKG_config.update({"libwine":"libwine_pkg"})
			self.PKG_config.update({"wine-etersoft":"wine-etersoft_pkg"})
		elif self.type_distrib=="hasp":
			self.names_pkg = ["haspd"]
			self.PKG_config.update({"haspd":"hasp_pkg"})
		elif self.type_distrib=="font":
			self.names_pkg = ["fonts-ttf-ms"]
			self.PKG_config.update({"fonts-ttf-ms":"font_pkg"})
		# ключ - тип пакета, значение - имя пакета
		self.PKG_name = {}
		for name in self.names_pkg:
			self.PKG_name.update({name:""})
		# Создаем статусы пакетов в окне
		# Словарь статусов пакетов. Ключ - тип пакета, значение - лебел
		self.PKG_label_status = {}
		# Словарь имен пакетов. Ключ - тип пакета, значение - лебел
		self.PKG_label_name = {}
		box_status = self.xml.get_widget("vbox_pkg")
		box_name = self.xml.get_widget("vbox_pkg_name")
		for name in self.names_pkg:
			# Создаем бокс и в него пакуем 2 лейбла, имя и статус
			hbox = gtk.HBox(False, 0)
			label_pkg_name = gtk.Label()
			label_pkg_name.set_use_markup(True)
			label_pkg_name.set_markup("<b>" + name + "</b>")
			label_pkg_name.set_padding(20, 5)
			label_pkg_status = gtk.Label()
			label_pkg_status.set_use_markup(True)
			label_pkg_status.set_alignment(1,0.5)
			hbox.pack_start(label_pkg_name, False, False)
			hbox.pack_start(label_pkg_status, True, True)
			self.PKG_label_status.update({name:label_pkg_status})
			# Пакуем все в главный бокс
			box_status.pack_start(hbox, True, True)
			# Создаем и пакуем имена пакетов
			label_name = gtk.Label()
			label_name.set_use_markup(True)
			label_name.set_padding(10, 0)
			label_name.set_alignment(0,0.5)
			self.PKG_label_name.update({name:label_name})
			box_name.pack_start(label_name, False, False)
		
		# Поле ввода каталога
		self.entry_dir = self.xml.get_widget("entry_dir")
		self.select_dir = config.get("pkg_dir")
		if self.select_dir=="" or os.access(self.select_dir, os.F_OK | os.R_OK)==False:
			if os.access(os.getcwd()+"/PKG/", os.F_OK | os.R_OK):
				self.select_dir = os.getcwd() + "/PKG/"
			else:
				self.select_dir = os.getcwd()
		self.selected = ""	# Для окна выбора файлов
		# Устанавливаем начальное значение для директории
		self.entry_dir.set_text(self.select_dir)
		
		self.win_main.show_all()
		
	def run(self):
		gtk.main()
		return self.ret_val
	
#___________________________________________________________________________________________
# Обработчики кнопок	
	def back(self, name=None):
		self.win_main.destroy()
		gtk.main_quit()
		self.ret_val = -1


#___________________________________________________________________________________________
# Установка пакетов
	# По нажатию клавиши производим установку или обновление
	def next(self, name=None):
		# Запоминаем директорию в конфиге
		self.config.set("pkg_dir", self.entry_dir.get_text())
		# Закрываем окно
		self.win_main.destroy()
		self.GTK_update()
		# Выводим лицензионное соглашение для wine
		if self.type_distrib=="wine" and self.license_file!="":
			f = open(self.license_file, "r")
			line = f.readline()
			lic_text = ""
			decoder = codecs.lookup("koi8-r")[1]
			while line!="":
				lic_text = lic_text + decoder(line)[0]
				line = f.readline()
			xml_lic = gtk.glade.XML(self.config.get("glade_path")+"win_license.glade")
			win_lic = xml_lic.get_widget("win_license")
			xml_lic.get_widget("textview").get_buffer().set_text(lic_text)
			win_lic.show_all()
			ret = win_lic.run()
			win_lic.destroy()
			self.GTK_update()
			if ret!=-5:
				gtk.main_quit()
				self.ret_val = -1
				return
		# Установка и переустановка пакетов
		if self.type_do in [3,6]:
			command = self.config.get("module_path")+"wineGUI_script_pkg.py " + os.getcwd()
			if self.depend:
				command = command + "/config reinstall_dep "
			else:
				command = command + "/config reinstall "
		elif self.type_do==2:
			command = self.config.get("module_path")+"wineGUI_script_pkg.py " + os.getcwd()
			if self.depend:
				command = command + "/config install_dep "
			else:
				command = command + "/config install "
		# Обновление пакетов
		elif self.type_do in [5,8]:
			command = self.config.get("module_path")+"wineGUI_script_pkg.py " + os.getcwd() + "/config update "
		# Указываем тип пакетов и путь к ним
		command = command + self.type_distrib
		for name in self.names_pkg:
			if self.PKG_name[name]!="not_used" and self.PKG_name[name]!="":
				command = command + " " + self.PKG_name[name]
		# Определяем тип запуска
		type_run = self.config.get("type_root")
		if type_run=="root":
			ret = os.system(command)
		elif type_run=="kdesu":
			ret = os.system("kdesu -d -n -c '" + command + "'")
		elif type_run=="gtksu":
			ret = os.system("gtksu '" + command + "'")
		elif type_run=="gksu":
			ret = os.system("gksu '" + command + "'")
		else:
			ret=3
			text = "<b>" + self._("ERROR:\nIn your system not found kdesu, gtksu or gksu") + "\n"
			text = text + self._("For Install, Reinstall and Updaye Packages need ROOT rights!") + "\n"
			text = text + self._("Please, restart program as ROOT") + "</b>"
			win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, "")
			win.set_markup(text)
			win.run()
			win.destroy()
		if ret==3:
			gtk.main_quit()
			self.ret_val = 1
			return

		gtk.main_quit()
		self.ret_val = 1
	
	
#___________________________________________________________________________________________
# Клик по кнопке выбора файла
	def dir_select(self, but):
		win = gtk.FileChooserDialog(but.name, self.win_main, gtk.FILE_CHOOSER_ACTION_SELECT_FOLDER, (gtk.STOCK_CANCEL, gtk.RESPONSE_REJECT, gtk.STOCK_OK, gtk.RESPONSE_ACCEPT))
		win.connect("response", self.win_dir_select )
		#filefilter = gtk.FileFilter()
		#filefilter.set_name("con")
		#filefilter.add_pattern("*con*")
		#filefilter_all = gtk.FileFilter()
		#filefilter_all.set_name("All files")
		#filefilter_all.add_pattern("*")
		#win.add_filter(filefilter)
		#win.add_filter(filefilter_all)
		win.run()
		if self.selected!="":
			self.select_dir = self.selected
			self.entry_dir.set_text(self.select_dir)

	def win_dir_select(self, dialog, resp_id):
		if resp_id == gtk.RESPONSE_REJECT or resp_id==-4:
			dialog.destroy()
			self.selected = ""
			return
		if resp_id == gtk.RESPONSE_ACCEPT:
			self.selected = dialog.get_filename()
			dialog.destroy()

#___________________________________________________________________________________________
# Функции проверки пакетов

	# Срабатывает при внесении изменений в поле каталога
	def change_dir(self, wid):
		for name in self.names_pkg:
			self.PKG_name[name]=""
			# Показываем юзеру имена пакетов
			self.PKG_label_name[name].set_markup("")

		# Смотрим, есть ли вообще такой каталог
		pkg_dir = self.entry_dir.get_text()
		if os.access(pkg_dir, os.F_OK | os.R_OK):
			# Ищем в этом каталоге info файл с нужным нам дистрибутивом
			all_file = os.listdir(pkg_dir)
			for item in all_file:
				if ".info" in item:
					if self.scan_file(item, pkg_dir):
						return
			# Если не нашли, то ищем во вложенных каталогах
			for item in all_file:
				try:
					in_dir = pkg_dir+"/"+item
					in_list_dir = os.listdir(in_dir)
					for file in in_list_dir:
						if ".info" in file:
							if self.scan_file(file, in_dir):
								return
				except:
					# item not dir
					pass
	
		self.set_status()
		# Если пакеты не найдены, блокируем кнопку Next
		self.but_next.set_sensitive(False)
			
	# Определяет, является ли info файл тем, что нам нужно, и вытаскивает всю инфу из него
	# Возвращает True, если файл правильный
	# Разблокирует кнопку Next, если все пакеты найдены
	def scan_file(self, file_name, dir_name):
		try:
			file = open(dir_name + "/" + file_name, "r")
		except:
			return False
		lines = file.readlines()
		# Проверяем, наш ли это дистрибутив
		n = 0
		for line in lines:
			# Название
			if "linux_distrib" in line:
				if not(self.config.get("linux_distrib") in line):
					return False
				n = n + 1
			# Проверяем, наша ли это версия Linux
			elif "linux_version" in line:
				if not(self.config.get("linux_version") in line):
					return False
				n = n + 1
			# Проверяем версию wine
			elif "wine_version" in line:
				if not(self.config.get("gui_version") in line):
					return False
				n = n + 1
			# Тот ли тип продукта
			elif "wine_name" in line:
				if not(self.config.get("wine_name") in line):
					return False
				n = n + 1
		if n!=4:
			return False
		
		# Ищем имена пакетов
		for name in self.names_pkg:
			self.PKG_name[name]=""
		for line in lines:
			for name in self.names_pkg:
				if self.PKG_config[name] in line:
					self.PKG_name[name] =self.get_opt(line, self.PKG_config[name])
		# Защита от ошибки "Все не используемые"
		all_not_used = True
		for name in self.names_pkg:
			if self.PKG_name[name]!="not_used":
				all_not_used = False
		if all_not_used:
			for name in self.names_pkg:
				self.PKG_name[name]=""
			return False
		# Если хоть один не нашли - возвращаемся
		if "" in self.PKG_name.values():
			return False
		
		# Показываем юзеру имена пакетов
		for name in self.names_pkg:
			if self.PKG_name[name]!="not_used":
				self.PKG_label_name[name].set_markup("(" + self.PKG_name[name] + ")")

		# Пытаемся найти файл с лицензией
		self.license_file = ""
		for line in lines:
			if "license_file" in line:
				self.license_file = self.get_opt(line, "license_file")
				break

		# Ищем hash файл и определяем контрольные суммы пакетов
		file_ok = False
		for line in lines:
			if "hash_file" in line:
				hash_file_name = self.get_opt(line, "hash_file")
				if hash_file_name!="":
					try:
						hash_file = open(dir_name + "/" + hash_file_name, "r")
						file_ok = True
					except:
						return False
					all_line = hash_file.readlines()
					for name in self.names_pkg:
						if self.PKG_name[name]!="not_used":
							hash_pkg = ""
							for item in all_line:
								if " "+self.PKG_name[name] in item:
									hash_pkg = self.get_opt(item, self.PKG_name[name])
									break
							# Проверяем на целостность
							self.PKG_name[name] = dir_name + "/" + self.PKG_name[name]
							test = self.check_file(self.PKG_name[name], hash_pkg)
							if test==0:
								self.PKG_name[name] = ""
							elif test==1:
								self.PKG_name[name] = "faulty"
					# Проверяем файл лицензии
					if self.license_file!="":
						hash_pkg = ""
						for item in all_line:
							if " "+self.license_file in item:
								hash_pkg = self.get_opt(item, self.license_file)
								break
						# Проверяем на целостность
						self.license_file = dir_name + "/" + self.license_file
						test = self.check_file(self.license_file, hash_pkg)
						if test!=2:
							self.license_file = ""
				break
		if not file_ok:
			for name in self.names_pkg:
				self.PKG_name[name] = "faulty"
			return False
		
		if ("" in self.PKG_name.values()) or ("faulty" in self.PKG_name.values()):
			return False
		
		self.set_status()
		self.but_next.set_sensitive(True)

		return True

	# Устанавливает состояния поиска пакетов
	def set_status(self):
		for name in self.names_pkg:
			if self.PKG_name[name]!="":
				if self.PKG_name[name]=="not_used":
					self.PKG_label_status[name].set_markup("<b>" + self._("Not used") + "</b>")
				elif self.PKG_name[name]=="faulty":
					self.PKG_label_status[name].set_markup("<b><span foreground='red'>" + self._("Faulty") + "</span></b>")
				else:
					self.PKG_label_status[name].set_markup("<b><span foreground='blue'>" + self._("Found") + "</span></b>")
			else:
				self.PKG_label_status[name].set_markup("<b><span foreground='red'>" + self._("Not found") + "</span></b>")
		
	# Возвращает значение опции option
	def get_opt(self, string, option):
		string = string.expandtabs(1)
		string = string.replace(" ", "")
		string = string.replace("\n", "")
		string = string.replace("=", "")
		return string.replace(option, "")
	
	# Проверяет, существует ли файл, и сходится ли его контрольная сумма. Возвращает
	# 0 - Файл не найден
	# 1 - файл найден, но поврежден
	# 2 - все ок
	def check_file(self, file_name, file_hash):
		# Существует ли файл
		if os.access(file_name, os.F_OK | os.R_OK):
			# Совпадает ли контрольная сумма
				com = popen2.Popen3("md5sum " + file_name)
				ret = com.wait()
				if ret==0:
					sum = com.fromchild.readline()
					sum = self.get_opt(sum, file_name)
					if sum==file_hash:
						return 2
					else:
						return 1
				else:
					return 2
		return 0


	def GTK_update(self):
		gobject.timeout_add(200, self.wait_events)
		gtk.main()
		
	def wait_events(self):
		if gtk.events_pending()==0:
			gtk.main_quit()
			return False
		return True

