#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Класс главного окна программы
import gtk, gtk.glade, gtk.gdk
import os, gobject
import wineGUI_win_install_wine
import wineGUI_win_wine_env
import wineGUI_win_config_wine
import wineGUI_win_diag_wine
import wineGUI_functions_pkg
import wineGUI_help_text
import wineGUI_win_help_install
import codecs

class main_window:
	
	def __init__(self, config):
		self.config = config
		self._ = self.config.translate
		self.module = config.get("module_path")
		glade_path = config.get("glade_path")
		self.xml = gtk.glade.XML(glade_path+"/win_main.glade")
		self.xml.signal_autoconnect(self)
		self.win_main = self.xml.get_widget("win_main")
		
		# Стиль
		BG_COLOR = "#AAEEFF"
		HELP_BG_COLOR = "#AACCFF"
		self.TIME_FOR_HELP = 500
		self.TIME_FOR_DOC = 5000

		# Устанавливаем цвет фона
		#self.set_color(BG_COLOR)
		
		# Оформляем текстовые окна
		self.xml.get_widget("scrolled_main").modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse("black"))
		self.xml.get_widget("viewport_help").modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse("black"))
		text_help = self.xml.get_widget("textview_help")
		#self.xml.get_widget("viewport").modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse("gray"))
		#text_help.modify_base(gtk.STATE_NORMAL, gtk.gdk.color_parse(HELP_BG_COLOR))
		self.buf_help = text_help.get_buffer()
		
		# Название и версия продукта
		string = '<span size="16000">Wine@Etersoft ' + config.get("wine_name") + ' v.' + config.all["gui_version"] + '</span>'
		self.xml.get_widget("label_product").set_markup(string)
		# Серийный номер
		string = '<span size="12000">' + self._("Registration number") + ": " + config.get("serial") + "</span>"
		self.xml.get_widget("label_number").set_markup(string)
		
		# Добавляем кнопку проверки подлинности
		but = gtk.Button(self._("Establish authenticity"))
		but.set_name("but_establish")
		but.set_size_request(-1, 40)
		but.connect("clicked", self.but_establish)
		but.connect("enter_notify_event", self.mouse_on)
		but.connect("leave_notify_event", self.mouse_off)
		box = self.xml.get_widget("vbox_button")
		box.pack_start(but, False, False)
		box.reorder_child(but, 5)
		
		# Надписи на главном окне
		buf_main = self.xml.get_widget("textview_main").get_buffer()
		text_main = "\n"
		if self.config.get("test_mode")=="True":
			text_main = text_main + self._("Program in test mode") + "\n"
		else:
			text_main = text_main + self._("Full version of program") + "\n"
		decoder = codecs.lookup("koi8-r")[1]
		text_main = text_main + self._("Owner") + ": " + decoder(self.config.get("owner"))[0] + "\n"
		text_main = text_main + "\n\n\n\n\n"
		text_main = text_main + self._("(c) LLC 'Etersoft'") + "\n"
		text_main = text_main + self._("Russian, Saint-Petersburg 2007") + "\n"
		text_main = text_main + "http://www.etersoft.ru"
		buf_main.set_text(text_main)

		text = wineGUI_help_text.text_win_main["DOC"]
		self.buf_help.set_text(self._(text))
		
		self.help_time = None
		
		self.win_main.show_all()
		
	def run(self):
		gtk.main()
	
	def win_destroy(self, name=None, param=None):
		self.win_main.destroy()
		gtk.main_quit()
	
#___________________________________________________________________________________________
# Обработка кнопок и тулбара
	
	# Управление пакетами
	def but_install(self, button):
		self.win_main.hide()
		wine_consist = wineGUI_functions_pkg.is_wine_install()
		# Проверяем, установлен ли наш wine
		if wine_consist[0]>0:
			if wine_consist[3]!=self.config.get("serial"):
				text = self._("Warning! Serial number of wine distribution and installed wine mismatch!")
				win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_OK, "")
				win.set_markup(text)
				win.run()
				win.destroy()
		win_com = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_NONE, self._("Testing system..."))
		win_com.show_all()
		self.GTK_update()
		first_win = wineGUI_win_install_wine.win_install(self.config)
		win_com.destroy()
		self.GTK_update()
		ret = first_win.run()
		self.win_main.show_all()
		
	# Управление wine-окружением
	def but_wine_env(self, button):
		self.win_main.hide()
		win = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_NONE, self._("Testing system..."))
		win.show_all()
		self.GTK_update()
		first_win = wineGUI_win_wine_env.win_wine_env(self.config)
		win.destroy()
		self.GTK_update()
		ret = first_win.run()
		self.win_main.show_all()
		
	def but_configure(self, but):
		wine_consist = wineGUI_functions_pkg.is_wine_install()
		# Проверяем, установлен ли наш wine
		if wine_consist[0]<=0:
			text = self._("Error: Wine@Etersoft not install! This function not available")
			win = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, "")
			win.set_markup(text)
			win.run()
			win.destroy()
			return
		self.win_main.hide()
		win = wineGUI_win_config_wine.win_config_wine(self.config)
		ret = win.run()
		self.win_main.show_all()
		
	def but_diag(self, but):
		wine_consist = wineGUI_functions_pkg.is_wine_install()
		# Проверяем, установлен ли наш wine
		if wine_consist[0]<=0:
			text = self._("Error: Wine@Etersoft not install! This function not available")
			win = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, "")
			win.set_markup(text)
			win.run()
			win.destroy()
			return
		self.win_main.hide()
		win = wineGUI_win_diag_wine.win_diag_wine(self.config)
		ret = win.run()
		self.win_main.show_all()
		
	def but_win_prog(self, but):
		self.win_main.hide()
		win = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_NONE, self._("Search help-files..."))
		win.show_all()
		self.GTK_update()
		win_help = wineGUI_win_help_install.win_help_install(self.config)
		win.destroy()
		self.GTK_update()
		ret = win_help.run()
		self.win_main.show_all()
		
	# Проверка подлинности
	def but_establish(self, but):
		wine_consist = wineGUI_functions_pkg.is_wine_install()
		# Проверяем, установлен ли наш wine
		if wine_consist[0]>0:
			# Сверяем серийный номер
			if wine_consist[3]!=self.config.get("serial"):
				text = self._("ERROR! Serial number of wine distribution and installed wine mismatch! Establish authenticity is failure")
				win = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, "")
				win.set_markup(text)
				win.run()
				win.destroy()
				return
		self.win_main.hide()
		win_com = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_NONE, self._("Opening browser..."))
		win_com.show_all()
		self.GTK_update()
		# Варианты браузеров
		browsers = ["firefox", "konqueror", "mozilla", "seamonkey", "opera"]
		# Ищем браузер, который определен в системе
		browser = os.getenv("BROWSER")
		if browser!="":
			self.config.set("browser", browser)
		def_brow = self.config.get("browser")
		# Сначало пробуем в дефолтном
		if def_brow!="":
			command = def_brow + " http://sales.etersoft.ru/product/" + self.config.get("serial")
			ret = os.system(command)
			if ret==0:
				win_com.destroy()
				self.win_main.show_all()
				return
		# Если дефолтным не получилось, пробуем остальные
		for browser in browsers:
			if browser!=def_brow:
				command = browser + " http://sales.etersoft.ru/product/" + self.config.get("serial")
				ret = os.system(command)
				if ret==0:
					# Сохраняем новый дефолтный браузер
					self.config.set("browser", browser)
					self.config.write_config()
					self.win_main.show_all()
					win_com.destroy()
					return
		# Если ни один не подошел
		win_com.destroy()
		text = self._("ERROR! Can not open browser! For Establish authenticity go to the link")
		text = text + ": <span foreground='blue'>http://sales.etersoft.ru/product/" + self.config.get("serial") + "</span>"
		win = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, "")
		win.set_markup(text)
		win.run()
		win.destroy()

		self.win_main.show_all()
		return

		
		

		
#___________________________________________________________________________________________
# Система подсказок
	
	# При наведении мышки на объект - стартуем таймер, если мышка задержалась на объекте
	# более чем на секунду - заменяем текст в окне подсказки
	def mouse_on(self, wid, param=None):
		if self.help_time!=None:
			gobject.source_remove(self.help_time)
		self.help_time = gobject.timeout_add(self.TIME_FOR_HELP, self.show_help, wid.name)
	
	def mouse_off(self, wid, param=None):
		if self.help_time!=None:
			gobject.source_remove(self.help_time)
		self.help_time = gobject.timeout_add(self.TIME_FOR_DOC, self.show_help, "DOC")
	
	# При срабатывании таймеров меняем содержимое окна
	def show_help(self, name):
		self.help_time=None
		self.buf_help.set_text(self._(wineGUI_help_text.text_win_main[name]))
		return False
	
	
#___________________________________________________________________________________________
# К оформлению

	def GTK_update(self):
		gobject.timeout_add(200, self.wait_events)
		gtk.main()
		
	def wait_events(self):
		if gtk.events_pending()==0:
			gtk.main_quit()
			return False
		return True
	
	def set_color(self, bg_color):
		# линии
		self.xml.get_widget("line_up_black").modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse("black"))
		self.xml.get_widget("line_down_black").modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse("black"))
		self.xml.get_widget("line_left_black").modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse("black"))
		self.xml.get_widget("line_right_black").modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse("black"))
		self.xml.get_widget("line_up_color").modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse(bg_color))
		self.xml.get_widget("line_down_color").modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse(bg_color))
		self.xml.get_widget("line_left_color").modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse(bg_color))
		self.xml.get_widget("line_right_color").modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse(bg_color))
		# Главный фон
		self.xml.get_widget("color_bg").modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse(bg_color))
	
	