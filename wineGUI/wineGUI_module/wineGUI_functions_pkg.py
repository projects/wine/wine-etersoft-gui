#!/usr/bin/env python
# -*- coding: utf-8 -*-

import popen2

# Скрипты установки, удаления и проверки wine


# Функция определяющая, установлен ли wine
# Возвращает 0, если не установлен
# 1 - Если установлен платный wine-etersoft
# 2 - Если установлен бесплатный wine-etersoft
# -1 - если установлен какой-то другой wine, который нельзя обновить
def is_wine_install():
	com = popen2.Popen3("wine-glibc --version", True)
	ret = com.wait()
	if ret!=0:
		return [0, None, None, None]
	lines = com.fromchild.readlines()
	for line in lines:
		if "WINE@Etersoft" in line:
			# Определяем серийный номер
			ind = line.find("registration number is")
			if ind>0:
				try:
					serial = line[ind+23:ind+32:]
				except:
					serial = "---"
			else:
				serial = "---"
			# Определяем версию
			ver = None
			start = line.find("(")
			if start>0:
				end = line.find(")", start)
				ver = line[start+1:end:]
			if "SQL" in line:
				return [1, ver, "SQL", serial]
			elif "Network" in line:
				return [1, ver, "Network", serial]
			elif "Local" in line:
				return [1, ver, "Local", serial]
			else:
				return [2, ver, "Free", serial]
	return [-1, None, None, None]


# Функция определяющая, установлен ли hasp-драйвер
# Версия не определяется
def is_hasp_install():
	com = popen2.Popen3("/etc/init.d/haspd", True)
	ret = com.wait()
	if ret!=0:
		return False
	return True



# Сравнивает версии wine. Если вторая больше первой - вернет 1, если равны - 0, если вторая меньше, то -1
def is_new(old, new):
	if old=="" or new=="":
		return 1
	old_left = old.find(".")
	old_right = old.rfind(".")
	old_ver = [int(old[:old_left:]), int(old[old_left+1:old_right:]), int(old[old_right+1::])]
	
	new_left = new.find(".")
	new_right = new.rfind(".")
	new_ver = [int(new[:new_left:]), int(new[new_left+1:new_right:]), int(new[new_right+1::])]
	
	if old_ver == new_ver:
		return 0
	
	if old_ver[0] < new_ver[0]:
		return 1
	if old_ver[1] < new_ver[1]:
		return 1
	if old_ver[2] < new_ver[2]:
		return 1
	return -1

# Проверяем, установлен ли пакет name. Параметр - тип пакетов
# Возвращает список установленных пакетов или [], если пакетов нет
# Если возникла ошибка, возвращаем строку
def check_pkg(type_pkg, name):
	if type_pkg=="rpm":
		com = popen2.Popen3("rpm -qa | grep " + name, True)
		ret = com.wait()
		if ret!=0:
			err = com.childerr.readlines()
			if ret==256 and err==[]:
				return []
			string = ""
			for line in err:
				string = string + line
			return string
		lines = com.fromchild.readlines()
		if lines==[]:
			return lines
		# Удаляем знаки переноса
		k=0
		try:
			while(1):
				lines[k]=lines[k].replace("\n", "")
				k = k + 1
		except:
			# конец цикла
			pass
		return lines
		
	elif type_pkg=="deb":
		com = popen2.Popen3("dpkg-query -l '*" + name + "*'", True)
		ret = com.wait()
		if ret!=0:
			err = com.childerr.readlines()
			string = ""
			for line in err:
				string = string + line
			return string
		lines = com.fromchild.readlines()
		if lines==[]:
			return lines
		# Удаляем знаки переноса
		k=0
		ret_line = []
		try:
			while(1):
				# Числится ли установленным
				if lines[k][0:2:]=="ii":
					# Выдергиваем только имя пакета, без версии
					lines[k]=lines[k][2::]
					lines[k]=lines[k].replace("\n", "")
					lines[k]=lines[k].strip()
					lines[k]=lines[k].expandtabs(1)
					ind = lines[k].find(" ")
					if ind>=0:
						ret_line.append(lines[k][:ind:])
				k = k + 1
		except:
			# конец цикла
			pass
		return ret_line
		
		
# Удаление пакетов wine
# На вход подается тип пакетов и список имен пакетов
# Возвращает два списска: пакеты, которые не удалось удалить и строки с ошибками
def remove_pkg(type_pkg, list_pkg):
	if type_pkg=="rpm":
		command = "rpm -e --noscripts"
		for pkg in list_pkg:
			command = command + " " + pkg
	
	elif type_pkg=="deb":
		command = "dpkg -P"
		for pkg in list_pkg:
			command = command + " " + pkg
	com = popen2.Popen3(command, True)
	ret = com.wait()
	error = com.childerr.readlines()
	pkg_not_del = []
	for pkg in list_pkg:
		if pkg in check_pkg(type_pkg, pkg):
			pkg_not_del.append(pkg)
	return [pkg_not_del, error]


# Функция установки wine
# На вход подается тип пакетов и имя пакета
# Возвращает успешность установки и строку с ошибкой
# 0 - все ОК
# 1 - поставилось с ошибками
# 2 - не поставилось
def install_pkg(type_pkg, pkg):
	if type_pkg=="rpm":
		command = "rpm -U " + pkg
	elif type_pkg=="deb":
		command = "dpkg -i " + pkg

	com = popen2.Popen3(command, True)
	ret = com.wait()
	error = com.childerr.readlines()
	if ret==0:
		if error==[]:
			return [0, error]
		else:
			return [1, error]
	else:
		return [2, error]


# Функция обновления пакетов
# На вход подается тип пакетов и имя пакета
# Возвращает успешность установки и строку с ошибкой
# 0 - все ОК
# 1 - поставилось с ошибками
# 2 - не поставилось
def update_pkg(type_pkg, list_pkg):
	if type_pkg=="rpm":
		command = "rpm -U "
	elif type_pkg=="deb":
		command = "dpkg --force-all -i "
	
	for pkg in list_pkg:
		command = command + pkg + " "
	
	com = popen2.Popen3(command, True)
	ret = com.wait()
	error = com.childerr.readlines()
	if ret==0:
		if error==[]:
			return [0, error]
		else:
			return [1, error]
	else:
		return [2, error]	












