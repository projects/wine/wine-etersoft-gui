#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gtk.glade
import gobject
import pwd, grp
import wineGUI_help_text

# Окно редактирования групп пользователей

class edit_group:
	
	def __init__(self, config, group, help):
		self._ = config.translate
		self.xml = gtk.glade.XML(config.get("glade_path")+"win_edit_group.glade")
		self.xml.signal_autoconnect(self)
		self.win_main = self.xml.get_widget("win_edit")
		
		# Окно подсказки 
		self.buf_help = self.xml.get_widget("textview_help").get_buffer()
		self.win_text_help = help
		self.buf_help.set_text(self.win_text_help)
		
		
		# Таймер для отображения подсказок
		self.help_time=None
		self.TIME_FOR_HELP = 500
		self.TIME_FOR_DOC = 5000
		
		self.tree_all = self.xml.get_widget("treeview_all")
		self.model_all = gtk.TreeStore(gobject.TYPE_STRING)
		self.tree_all.set_model(self.model_all)
		column = gtk.TreeViewColumn(self._("User name"), gtk.CellRendererText(), text=0)
		self.tree_all.append_column(column)
		
		self.tree_group = self.xml.get_widget("treeview_group")
		self.model_group = gtk.TreeStore(gobject.TYPE_STRING)
		self.tree_group.set_model(self.model_group)
		column = gtk.TreeViewColumn(self._("User name"), gtk.CellRendererText(), text=0)
		self.tree_group.append_column(column)
		
		# Признак нажатия отмены
		# 0 - все ок, 1 - нажали Отмена
		self.ret_val = 0

		# Начальное состояние группы
		# здесь сработает исключение, если группы нет, его следует отлавливать во внешнем обработчике
		tmp = grp.getgrnam("wineadmin")
		self.start_group = tmp[3]
		self.start_group.sort()
		# Начальное состояние юзеров. Не включаем сюда юзеров, которые есть в группе
		self.start_users = []
		tmp_user = pwd.getpwall()
		for user in tmp_user:
			if user[0] not in self.start_group:
				self.start_users.append(user[0])
		self.start_users.sort()
		
		# Заполняем Treeview
		# Юзеры
		for user in self.start_users:
			row = self.model_all.insert_before(None, None)
			self.model_all.set_value(row, 0, user)
		# Группа
		for user in self.start_group:
			row = self.model_group.insert_before(None, None)
			self.model_group.set_value(row, 0, user)
		
		
		
		self.win_main.show_all

	# Возвращаемое значение: [список_добавляемых_в_группу_юзеров, список_удаляемых_из_группы_юзеров, конечное_количество_юзеров_в_группе]
	# если возвращаем [], то нажали Отмена
	# если ничего не менять, возвращаем [[],[], num]
	def run(self):
		gtk.main()
		if self.ret_val==1:
			return []
		# Получаем список новой группы
		end_group = []
		iter_group = self.model_group.get_iter_first()
		num = 0
		while(iter_group!=None):
			end_group.append(self.model_group.get_value(iter_group, 0))
			iter_group = self.model_group.iter_next(iter_group)
			num = num + 1
		# Выявляем разницу
		ret_add = []
		ret_remove = []
		for name in end_group:
			if name not in self.start_group:
				ret_add.append(name)
		for name in self.start_group:
			if name not in end_group:
				ret_remove.append(name)
		return [ret_add, ret_remove, num]
		
		
		
		
	def on_cancel_clicked(self, wid, param=None):
		self.win_main.destroy()
		gtk.main_quit()
		self.ret_val = 1
		
	def on_ok_clicked(self, wid):
		self.win_main.destroy()
		gtk.main_quit()
		self.ret_val = 0
		
#___________________________________________________________________________________________
# Добавление и удаление из группы
	def add_to_group(self, wid, Param1=None, Param2=None):
		tree = self.tree_all.get_selection()
		model, iter_user = tree.get_selected()
		# Удаляем юзера из старой группы
		if iter_user:
			user = model.get_value(iter_user, 0)
			model.remove(iter_user)
			# заносим в новую
			row = self.model_group.insert_before(None, None)
			self.model_group.set_value(row, 0, user)


	def remove_from_group(self, wid, Param1=None, Param2=None):
		tree = self.tree_group.get_selection()
		model, iter_user = tree.get_selected()
		# Удаляем юзера из старой группы
		if iter_user:
			user = model.get_value(iter_user, 0)
			model.remove(iter_user)
			# заносим в новую
			row = self.model_all.insert_before(None, None)
			self.model_all.set_value(row, 0, user)

#___________________________________________________________________________________________
# Система подсказок
	def mouse_on(self, wid, param=None):
		if self.help_time!=None:
			gobject.source_remove(self.help_time)
		self.help_time = gobject.timeout_add(self.TIME_FOR_HELP, self.show_help, wid.name)
	
	def mouse_off(self, wid, param=None):
		if self.help_time!=None:
			gobject.source_remove(self.help_time)
		self.help_time = gobject.timeout_add(self.TIME_FOR_DOC, self.show_help, "DOC")

	def show_help(self, name):
		self.help_time=None
		text = ""
		if name=="DOC":
			text=self.win_text_help
		else:
			text = self._(wineGUI_help_text.text_edit_group[name])

		self.buf_help.set_text(text)
		return False




