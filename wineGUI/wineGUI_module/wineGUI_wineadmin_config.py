#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gtk.glade
import codecs
import wineGUI_win_edit_group
import wineGUI_win_show_log as Show_log
import grp, os
import popen2
import wineGUI_help_text

# Функции конфигурирования wine
# Создание wine-администратора
# Возвращает -1 в случае ошибки
# 0 - если отказались от создания
# 1 - если все ок
# Выполняется от имени root
def create_admin(config):
	_ = config.translate
	
	# Перекодировщик
	ret = codecs.lookup(config.get("code"))
	decoder = ret[1]
	
	# Выводим сообщеине с предложением указать wine-админа
	string = _(wineGUI_help_text.text_wineadmin_1)
	string = string + _("\tNow, in your system") + " "
	try:
		tmp = grp.getgrnam("wineadmin")
		if tmp[3]==[]:
			string = string + " " + _("no wine-administrator") + "\n"
		else:
			string = string + str(tmp[3].__len__()) + " " + _("wine-administrator") + "\n"
	except:
		string = string + " <span foreground='red'>" + _("no wineadmin group!") + "</span> " + _("For using Administration Install need create it. Group create automaticaly with install Wine@Etersoft")
		win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, "")
		win.set_markup(string)
		win.run()
		win.destroy()
		return -1
		
	string = string + _(wineGUI_help_text.text_wineadmin_2)
	string = string + "<span foreground='blue'>" + _("Do you want show and change wineadmin group?") + "\n</span>"
	win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_QUESTION, gtk.BUTTONS_YES_NO, "")
	win.set_markup(string)
	ret = win.run()
	win.destroy()
	if ret!=-8:
		return 0

	# Если согласились, запускаем функцию создания юзера-администратора
	while(True):
		win_group = wineGUI_win_edit_group.edit_group(config, "wineadmin", _(wineGUI_help_text.text_wineadmin_5))
		ret = win_group.run()
		if ret==[]:
			return 0
		if ret[2]==0:
			# Если нет юзеров в группе, переспрашиваем
			string = "<b><span foreground='red'>" + _("Error") + ":</span></b>\n"
			string = string + _("In group 'wineadmin' no users") + "\n"
			string = string + "\n<b><span foreground='blue'>" + _("Do you want select wine-administartor?") + "\n</span></b>"
			win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_QUESTION, gtk.BUTTONS_OK_CANCEL, "")
			win.set_markup(string)
			ret = win.run()
			win.destroy()
			if ret!=-8:
				return 0
		if ret[0:2:]==[[],[]]:
			string = "<b>" + _("Warning: Group wineadmin not changed") + "</b>\n"
			string = string + _(wineGUI_help_text.text_wineadmin_4)
			win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_OK, "")
			win.set_markup(string)
			win.run()
			win.destroy()
			return 1
		# Если все в порядке - переходим к редактированию групп
		else:
			break
	# Составляем команду:
	command = ""
	com_gk = []
	# Добавляем юзеров в группу
	for user in ret[0]:
		# Определяем список групп юзера
		com = popen2.Popen3("id -G " + user, True)
		ret_com = com.wait()
		if ret_com!=0:
			string = "<b>" + _("Error: can not add User") + " " + user + " " + _("to group wineadmin!") + "</b>\n"
			log = "Command: take user`s list of groups\n"
			log = log + "Error: Can not do command 'id -G user_name'\n"
			log = log + "Detailes:\n\n"
			for line in com.childerr.readlines():
				log = log + decoder(line)[0]
			Show_log.show_error(config, gtk.MESSAGE_ERROR, string, log)
			return -1
		groups = com.fromchild.readline()
		all_group = groups.split()
		command = command + "usermod -G "
		str_gk = "usermod -G "
		for group in all_group:
			try:
				gr_name = grp.getgrgid(int(group))[0]
			except:
				continue
			command = command + gr_name + ","
			str_gk = str_gk + gr_name + ","
		command = command + "wineadmin " + user + " && "
		str_gk = str_gk + "wineadmin " + user
		com_gk.append(str_gk)
	# Удаляем юзеров из группы
	for user in ret[1]:
		# Определяем список групп юзера
		com = popen2.Popen3("id -G " + user, True)
		ret_com = com.wait()
		if ret_com!=0:
			string = "<b>" + _("Error: can not add User") + " " + user + " " + _("to group wineadmin!") + "</b>\n"
			log = "Command: take user`s list of groups\n"
			log = log + "Error: Can not do command 'id -G user_name'\n"
			log = log + "Detailes:\n\n"
			for line in com.childerr.readlines():
				log = log + decoder(line)[0]
			Show_log.show_error(config, gtk.MESSAGE_ERROR, string, log)
			return -1
		groups = com.fromchild.readline()
		all_group = groups.split()
		command = command + "usermod -G "
		str_gk = "usermod -G "
		for group in all_group:
			try:
				gr_name = grp.getgrgid(int(group))[0]
			except:
				continue
			if gr_name=="wineadmin":
				continue
			command = command + gr_name + ","
			str_gk = str_gk + gr_name + ","
		command = command[:-1:]
		str_gk = str_gk[:-1:]
		command = command + " " + user + " && "
		str_gk = str_gk + " " + user
		com_gk.append(str_gk)
	command = command + " exit 4"
	
	type_run = config.get("type_root")
	if os.getenv('USER')=="root" or os.getuid()==0:
		type_run="root"

	if type_run=="root":
		com = popen2.Popen3(command, True)
		ret_com = com.wait()
	
	elif type_run=="kdesu":
		com = popen2.Popen3("kdesu -d -n -c '" + command + "'", True)
		ret_com = com.wait()
		
	elif type_run=="gtksu":
		com = popen2.Popen3("gtksu '" + command + "'", True)
		ret_com = com.wait()
	elif type_run=="gksu":
		for com in com_gk:
			print com
			com = popen2.Popen3("gksu '" + com + "'", True)
			ret_com = com.wait()
		ret_com = 1024
	else:
		text = "<b>" + _("ERROR:\nIn your system not found kdesu, gtksu or gksu") + "\n"
		text = text + _("For modyfy group wineadmin need ROOT rights!") + "\n"
		text = text + _("Please, restart program as ROOT") + "</b>"
		win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, "")
		win.set_markup(text)
		win.run()
		win.destroy()
		return -1

	if ret_com==0:
		string = "<b>" + _("Error: Group change canceled") + "</b>"
		win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, "")
		win.set_markup(string)
		win.run()
		win.destroy()
		return -1

	if ret_com!=1024:
		string = "<b>" + _("Error: can not change group wineadmin!") + "</b>\n"
		log = "Command: change group wineadmin\n"
		log = log + "Error: Can not change group wineadmin\n"
		log = log + "Detailes:\n\n"
		log = log + decoder(com.childerr.readline())[0]
		Show_log.show_error(config, gtk.MESSAGE_ERROR, string, log)
		return -1
	
	string = "<b>" + _("Group changed succesfully") + "</b>\n"
	string = string + _(wineGUI_help_text.text_wineadmin_3)
	string = string + _(wineGUI_help_text.text_wineadmin_4)
	win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_OK, "")
	win.set_markup(string)
	win.run()
	win.destroy()
	
	return 1


