#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gtk, gtk.glade
import gobject
import os
import string
import wineGUI_help_text

# Класс окна редактирования общих wine-ресурсов

class win_edit_disk:
	
	def __init__(self, config):
		self.config = config
		self._ = self.config.translate
		
		self.xml = gtk.glade.XML(config.get("glade_path")+"win_edit_disk.glade")
		self.xml.signal_autoconnect(self)
		self.win_main = self.xml.get_widget("win_edit_disk")
		# имя файла конфигурации общих ресурсов
		self.file_res = "/etc/wine/map_resources.sh"
		self.file_default = "/etc/wine/map_devices.sh"

		# Окно подсказок
		self.buf_help = self.xml.get_widget("text_help").get_buffer()
		text = wineGUI_help_text.text_edit_disk["DOC"]
		self.buf_help.set_text(self._(text))
		self.help_time=None
		self.TIME_FOR_HELP = 500
		self.TIME_FOR_DOC = 5000
		
		# Список ресурсов
		self.tree_res = self.xml.get_widget("treeview")
		self.model = gtk.TreeStore(gobject.TYPE_STRING, gobject.TYPE_STRING, gobject.TYPE_STRING)
		self.tree_res.set_model(self.model)
		column = gtk.TreeViewColumn(self._("Resource name"), gtk.CellRendererText(), text=0)
		column.set_resizable(False)
		column.set_min_width(150)
		self.tree_res.append_column(column)
		column = gtk.TreeViewColumn(self._("Resource real adress"), gtk.CellRendererText(), text=1)
		column.set_resizable(False)
		column.set_min_width(350)
		self.tree_res.append_column(column)
		column = gtk.TreeViewColumn(self._("Resouce type"), gtk.CellRendererText(), text=2)
		column.set_max_width(100)
		column.set_resizable(False)
		self.tree_res.append_column(column)

		# Словарь ресурсов. Ключ - название (win), значение - [реальный адресс, тип]
		# Вычитываем конфигурацию
		self.all_res = self.check_file(self.file_res)
		# Вычитываем системную стандартную конфигурацию
		used_res = self.check_file(self.file_default)
		for key in used_res.keys():
			used_res[key] = [self._("can not modyfy"), used_res[key][1]]
		self.all_res.update(used_res)
		self.all_res.update({"C":[self._("can not modyfy"), self._("Disc")]})
		
		# Заполняем список
		keys = self.all_res.keys()
		keys.sort()
		for key in keys:
			row = self.model.insert_before(None, None)
			self.model.set_value(row, 0, key)
			self.model.set_value(row, 1, self.all_res[key][0])
			self.model.set_value(row, 2, self.all_res[key][1])

		# Устанавливаем начальные значения
		self.label_res = self.xml.get_widget("label_disk")
		self.label_res.set_text(self._("Letter win-disk (A-Z)"))
		
		self.entry_name_res = self.xml.get_widget("entry_name_res")
		self.entry_real_adress = self.xml.get_widget("entry_real_adress")
		

		# Выбор типа ресурса
		self.ComBox = gtk.combo_box_new_text()
		self.xml.get_widget("vbox_combobox").pack_start(self.ComBox)
		self.ComBox.append_text(self._("Disc"))
		self.ComBox.append_text(self._("Network"))
		self.box_type = [self._("Disc"), self._("Network")]
		self.ComBox.connect("changed", self.switch_com)
		self.ComBox.set_active(0)
		
		# Кнопки
		self.but_select_dir = self.xml.get_widget("but_select_dir")
		self.but_edd_res = self.xml.get_widget("but_edd_res")
		self.but_update_res = self.xml.get_widget("but_update_res")
		self.but_remove_res = self.xml.get_widget("but_remove_res")
		
		# Для выбора каталога
		self.selected = None
		
		# Текущий выдененный ресурс
		self.current_key = ""

		self.win_main.show_all()
		
	def run(self):
		gtk.main()
		err = "<b>" + self._("Warning! For apply this changes you must recreate wine-environment!") + "</b>"
		win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_OK, "")
		win.set_markup(err)
		win.run()
		win.destroy()



	def check_file(self, file_name):
		if os.access(file_name, os.F_OK | os.R_OK):
			try:
				f = open(file_name, "r")
				ret_dic = {}
				line = f.readline()
				while line!="":
					# Выделяем значимые строки
					line = line.expandtabs(1)
					line = line.strip()
					if line=="" or line=="/n":
						line = f.readline()
						continue
					if line[0]=="#":
						line = f.readline()
						continue
					ind = line.find("map_drive")
					if ind < 0:
						line = f.readline()
						continue
					line = line[ind + 9::]
					line = line.strip()
					real_address, name = line.split()[0:2:]
					if ":" in name:
						name = name.upper()
						if (name.replace(":","")).__len__() == 1:
							type_res = self._("Disc")
						else:
							line = f.readline()
							continue
					else:
						ind = name.find("unc")
						if ind < 0:
							# неправильный ресурс
							line = f.readline()
							continue
						name = name[ind+4::]
						name = name.replace("/", "\\")
						name = "\\\\" + name
						type_res = self._("Network")
					ret_dic.update({name:[real_address, type_res]})
					line = f.readline()
				f.close()
				return ret_dic
			except:
				# Битый файл
				pass
		return {}


#___________________________________________________________________________________________
# Обработчики кнопок	
	
	# Кнопка выход
	def on_but_exit(self, wid, param=None):
		self.win_main.destroy()
		gtk.main_quit()


	# Кнопка Добавить
	def but_edd_res(self, but):
		name = self.entry_name_res.get_text()
		name = name.replace(":", "")
		self.entry_name_res.set_text(name)
		if self.check_all():
			adress = self.entry_real_adress.get_text()
			type_res = self.box_type[self.ComBox.get_active()]
			if type_res==self._("Disc"):
				name = name.upper()
				self.entry_name_res.set_text(name)
				name = name + ":"
			else:
				if name[-1::]=="\\":
					name = name[:-1:]
					self.entry_name_res.set_text(name)
			# Если добавляем существующее - то ошибка
			if name in self.all_res.keys():
				err = "<b>" + self._("Error") + ": '" + name + "' " + self._("already exist") + "</b>"
				win = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, "")
				win.set_markup(err)
				win.run()
				win.destroy()
				return
			win = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_QUESTION, gtk.BUTTONS_YES_NO, self._("Are you sure add public default resource?"))
			ret = win.run()
			win.destroy()
			if ret!=-8:
				return False
			self.all_res.update({name:[adress, type_res]})
			if self.update_file()==False:
				self.all_res.__delitem__(name)
			else:
				row = self.model.insert_before(None, None)
				self.model.set_value(row, 0, name)
				self.model.set_value(row, 1, adress)
				self.model.set_value(row, 2, type_res)
	
	# Кнопка Обновить
	def but_update_res(self, but):
		# Обновлять будем выделенное
		tree = self.tree_res.get_selection()
		model, iter_res = tree.get_selected()
		if iter_res:
			key = model.get_value(iter_res, 0)
		else:
			err = "<b>" + self._("Error: No selected object") + "</b>"
			win = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, "")
			win.set_markup(err)
			win.run()
			win.destroy()
			return
		if self.check_all():
			name = self.entry_name_res.get_text()
			adress = self.entry_real_adress.get_text()
			type_res = self.box_type[self.ComBox.get_active()]
			if type_res==self._("Disc"):
				name = name.upper()
				self.entry_name_res.set_text(name)
				name = name + ":"
			else:
				if name[-1::]=="\\":
					name = name[:-1:]
					self.entry_name_res.set_text(name)
			# Нельзя обновить на существующее имя, кроме выделенного
			if key!=name and name in self.all_res.keys():
				err = "<b>" + self._("Error") + ": '" + name + "' " + self._("already exist") + "</b>"
				win = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, "")
				win.set_markup(err)
				win.run()
				win.destroy()
				return
			win = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_QUESTION, gtk.BUTTONS_YES_NO, self._("Are you sure modyfy public default resource?"))
			ret = win.run()
			win.destroy()
			if ret!=-8:
				return False

			prev_name = key
			prev_val = self.all_res[key]
			self.all_res.__delitem__(key)
			self.all_res[name] = [adress, type_res]
			if self.update_file()==False:
				self.all_res.__delitem__(name)
				self.all_res[prev_name] = prev_val
			else:
				model.set_value(iter_res, 0, name)
				model.set_value(iter_res, 1, adress)
				model.set_value(iter_res, 2, type_res)
	
	# Кнопка Удалить ресурс
	def but_remove_res(self, but):
		# Удалять будем выделенное
		tree = self.tree_res.get_selection()
		model, iter_res = tree.get_selected()
		if iter_res:
			key = model.get_value(iter_res, 0)
		else:
			err = "<b>" + self._("Error: No selected object") + "</b>"
			win = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, "")
			win.set_markup(err)
			win.run()
			win.destroy()
			return
		win = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_QUESTION, gtk.BUTTONS_YES_NO, self._("Are you sure remove public default resource?"))
		ret = win.run()
		win.destroy()
		if ret!=-8:
			return False
		prev_name = key
		prev_val = self.all_res[key]
		self.all_res.__delitem__(key)
		if self.update_file()==False:
			self.all_res.update({key:prev_val})
		else:
			model.remove(iter_res)

	# Реакция на выделение ресурса в списске
	def select_res(self, wid):
		tree = self.tree_res.get_selection()
		model, iter_res = tree.get_selected()
		if iter_res:
			self.current_key = model.get_value(iter_res, 0)
			if self.all_res[self.current_key][0]==self._("can not modyfy"):
				self.but_update_res.set_sensitive(False)
				self.but_remove_res.set_sensitive(False)
				self.entry_name_res.set_text("")
				self.entry_real_adress.set_text("")
			else:
				if self.all_res[self.current_key][1]==self._("Disc"):
					self.entry_name_res.set_text(self.current_key.replace(":", ""))
					self.ComBox.set_active(0)
					self.label_res.set_text(self._("Letter win-disk (A-Z)"))
				else:
					self.entry_name_res.set_text(self.current_key)
					self.ComBox.set_active(1)
					self.label_res.set_text(self._('Network path ( \\\\server\\dir )'))
				self.entry_real_adress.set_text(self.all_res[self.current_key][0])
				self.but_update_res.set_sensitive(True)
				self.but_remove_res.set_sensitive(True)

	# Реакция на смену Combobox
	def switch_com(self, wid):
		type_res = self.box_type[self.ComBox.get_active()]
		if type_res==self._("Disc"):
			self.label_res.set_text(self._("Letter win-disk (A-Z)"))
		else:
			self.label_res.set_text(self._('Network path ( \\\\server\\dir )'))
			
			
	# Кнопка выбора каталога
	def select_dir_real(self, but):
		win = gtk.FileChooserDialog(but.name, self.win_main, gtk.FILE_CHOOSER_ACTION_SELECT_FOLDER, (gtk.STOCK_CANCEL, gtk.RESPONSE_REJECT, gtk.STOCK_OK, gtk.RESPONSE_ACCEPT))
		win.connect("response", self.win_dir_select )
		win.run()
		if self.selected!="":
			self.entry_real_adress.set_text(self.selected)

	def win_dir_select(self, dialog, resp_id):
		if resp_id == gtk.RESPONSE_REJECT or resp_id==-4:
			dialog.destroy()
			self.selected = ""
			return
		if resp_id == gtk.RESPONSE_ACCEPT:
			self.selected = dialog.get_filename()
			dialog.destroy()



#___________________________________________________________________________________________
# Обработка данных с полей

	# Определяет правильность введенных значений
	def check_all(self):
		name = self.entry_name_res.get_text()
		adress = self.entry_real_adress.get_text()
		type_res = self.box_type[self.ComBox.get_active()]
		# Проверяем реальный адресс
		if adress=="":
			err = "<b>" + self._("Please, enter real adress of resource") + "</b>"
			win = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, "")
			win.set_markup(err)
			win.run()
			win.destroy()
			return False
		if not os.access(adress, os.F_OK):
			err = "<b>" + self._("Error: resource") + " '" + adress + "' " + self._("not found") + "</b>"
			win = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, "")
			win.set_markup(err)
			win.run()
			win.destroy()
			return False

		# Проверяем имя
		if type_res==self._("Disc"):
			if name.__len__()!=1 or (name not in string.letters):
				err = "<b>" + self._("Error name disk") + " '" + name + "': " + self._("must be one letter A-Z") + "</b>"
				win = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, "")
				win.set_markup(err)
				win.run()
				win.destroy()
				return False
		else:
			start_name = name
			err = False
			if name[0:2:] == "\\\\":
				name = name[2::]
				if name[0] != "\\" and name.count("\\\\")==0:
					if name[-1::]=="\\":
						name = name[:-1:]
					names = name.split("\\")
					valid = string.letters
					valid = valid + "_"
					valid_first = valid
					valid = valid + "." + string.digits
					for path in names:
						if path in ["", "_", "."]:
							err=True
							break
						if (path[0] not in valid_first):
							err=True
							break
						for letter in path[1::]:
							if letter not in valid:
								err = True
								break
				else:
					err=True
			else:
				err=True
			if err:
				err = "<b>" + self._("Error format of network name") + " '" + start_name + "': " + self._("must be\n'\\\\server_name\\resource_name'") + "</b>"
				win = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, "")
				win.set_markup(err)
				win.run()
				win.destroy()
				return False
		return True
	
	
	# Обновляет файл
	def update_file(self):
		# Готовим строки
		file_line = "# Don`t modyfy this file, it was ganerated automaticaly\n\n"
		for key, val in self.all_res.items():
			if val[0]==self._("can not modyfy"):
				continue
			if val[1]==self._("Disc"):
				file_line = file_line + "[ -d " + val[0] + " ] && map_drive " + val[0] + " " + key.lower() + "\n"
			else:
				name = key[2::]
				server = name.split("\\")
				serv_line = "~/.wine/dosdevices/unc"
				for path in server[:-1:]:
					serv_line = serv_line + "/" + path
				file_line = file_line + "[ -d " + serv_line + " ] || mkdir -p " + serv_line + "\n"
				file_line = file_line + "[ -d " + val[0] + " ] && map_drive " + val[0] + " unc"
				for path in server:
					file_line = file_line + "/" + path
				file_line = file_line + "\n"
			file_line = file_line + "\n"

		if os.access("/etc/wine/", os.F_OK)==0:
			text = self._("Error: path /etc/wine/ not found, can not modyfy public resources")
			win = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, "")
			win.set_markup(text)
			win.run()
			win.destroy()
			return False

		try:
			f = open("tmp_map_resources.tmp","w")
			f.write(file_line)
			f.close()
		except:
			text = self._("Error: Can not create temp-file for public resources!")
			win = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, "")
			win.set_markup(text)
			win.run()
			win.destroy()
			return False

		command = "cp -f tmp_map_resources.tmp /etc/wine/map_resources.sh && exit 4"

		type_run = self.config.get("type_root")
		
		if os.getenv('USER')=="root" or os.getuid()==0:
			type_run="root"

		if type_run=="root":
			ret = os.system(command)
			if ret!=1024:
				text = self._("Error: Can not modyfy public resources!")
				win = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, "")
				win.set_markup(text)
				win.run()
				win.destroy()
				os.remove("tmp_map_resources.tmp")
				return False
		elif type_run=="kdesu":
			ret = os.system("kdesu -d -n -c '" + command + "'")
			if ret!=1024:
				text = self._("Error: Can not modyfy public resources!")
				win = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, "")
				win.set_markup(text)
				win.run()
				win.destroy()
				os.remove("tmp_map_resources.tmp")
				return False
		elif type_run=="gtksu":
			ret = os.system("gtksu '" + command + "'")
		elif type_run=="gksu":
			ret = os.system("gksu 'cp -f tmp_map_resources.tmp /etc/wine/map_resources.sh'")
		else:
			text = "<b>" + self._("ERROR:\nIn your system not found kdesu, gtksu or gksu") + "\n"
			text = text + self._("For modyfy default public resource need ROOT rights!") + "\n"
			text = text + self._("Please, restart program as ROOT") + "</b>"
			win = gtk.MessageDialog(self.win_main, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, "")
			win.set_markup(text)
			win.run()
			win.destroy()
			os.remove("tmp_map_resources.tmp")
			return False
		
		#os.remove("tmp_map_resources.tmp")

		return True


#___________________________________________________________________________________________
# Система подсказок
	def mouse_on(self, wid, param=None):
		if self.help_time!=None:
			gobject.source_remove(self.help_time)
		self.help_time = gobject.timeout_add(self.TIME_FOR_HELP, self.show_help, wid.name)
	
	def mouse_off(self, wid, param=None):
		if self.help_time!=None:
			gobject.source_remove(self.help_time)
		self.help_time = gobject.timeout_add(self.TIME_FOR_DOC, self.show_help, "DOC")

	def show_help(self, name):
		self.help_time=None
		self.buf_help.set_text(self._(wineGUI_help_text.text_edit_disk[name]))
		return False





