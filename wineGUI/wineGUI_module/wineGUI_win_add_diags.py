#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gtk, gtk.glade
import gobject
import os, imp
#import codecs

# Класс окна запуска дополнительной диагностики

class win_add_diags:
	
	def __init__(self, config):
		self.config = config
		self._ = self.config.translate
		
		self.xml = gtk.glade.XML(config.get("glade_path")+"win_add_diags.glade")
		self.xml.signal_autoconnect(self)
		self.win_main = self.xml.get_widget("win_add_diags")

		# Окно программ
		self.tree_prog = self.xml.get_widget("treeview")
		self.model = gtk.TreeStore(gobject.TYPE_STRING)
		self.tree_prog.set_model(self.model)
		column = gtk.TreeViewColumn(self._("Test name"), gtk.CellRendererText(), text=0)
		self.tree_prog.append_column(column)
		
		# Выделенная в данный момент программа
		self.selected=""
		
		# Окно подсказки
		self.buf_help = self.xml.get_widget("textview").get_buffer()
		self.buf_help.set_text(self._("Please, select test from list and show info"))

		# словарь подсказок. Ключ - тип программы , значение - [Имя программы, имя файла]
		self.all_test = {}

		# Составляем список программ, для которых есть помощь
		test_dir = self.config.get("test_path")
#		ret = codecs.lookup("koi8-r")
#		decoder = ret[1]
		for item in os.listdir(test_dir):
			try:
				f = open(test_dir + "/" + item + "/about", "r")
				name = f.readline()[:-1:]
				helpname = f.readline()[:-1:]
				modname = f.readline()
				f.close()
				if name!="" and helpname!="" and modname!="":
					# Проверяем файл помощи
					helpname = test_dir + "/" + item + "/" + helpname
					if os.access(helpname, os.F_OK | os.R_OK)==0:
						continue
					# Проверяем указанный модуль
					try:
						mod = imp.load_source(modname[:-3:], test_dir + item + "/" + modname)
					except:
						continue
					self.all_test[name] = [helpname, mod]
			except:
				continue
		
		# Пакуем имена в список
		for name in self.all_test.keys():
			row = self.model.insert_before(None, None)
			self.model.set_value(row, 0, name)


		self.win_main.show_all()
		
	def run(self):
		gtk.main()

#___________________________________________________________________________________________
# Обработка событий

	def on_button_exit(self, but, param=None):
		self.win_main.destroy()
		gtk.main_quit()

	def select_prog(self, wid):
		tree = self.tree_prog.get_selection()
		model, iter_prog = tree.get_selected()
		text = ""
		if iter_prog:
			key = model.get_value(iter_prog, 0)
			try:
				f = open(self.all_test[key][0], "r")
				for line in f.readlines():
					text = text + line
				f.close()
				self.buf_help.set_text(text)
				self.selected = key
			except:
				self.selected = ""
				self.buf_help.set_text(self._("Sorry, can not show this help file: ") + self.all_test[key][0])
			
	def on_but_run(self, wid):
		if self.selected=="":
			text = self._("No selected test")
			win = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, text)
			win.run()
			win.destroy()
			return
	
		self.win_main.hide()
		self.GTK_update()
		try:
			self.all_test[self.selected][1].main(self.config)
		except:
			pass
		self.win_main.show_all()
		



#___________________________________________________________________________________________
# Для обновления окна

	def wait_events(self):
		if gtk.events_pending()==0:
			gtk.main_quit()
			return False
		return True
	
	def GTK_update(self):
		gobject.timeout_add(200, self.wait_events)
		gtk.main()



		
			
