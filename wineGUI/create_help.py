#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys

# Модуль для создания файлов помощи

types = ["Local", "Network", "SQL"]

# Обновляем все на всякий случай
ret = os.system("cvs update")
if ret != 0:
	print "Не удалось выполнить cvs update"
	sys.exit(1)


# Проверяем наличие файлов с исходными данными
for type_pr in types:
	if not os.access("help." + type_pr + ".src", os.F_OK | os.R_OK):
		print "Файл help." + type_pr + ".src не найден"
		sys.exit(1)

# Проверяем, есть ли все каталоги
for type_pr in types:
	if not os.access("win_help." + type_pr, os.F_OK | os.R_OK):
		print "Каталог win_help." + type_pr + ".src не найден"
		sys.exit(1)

if not os.access("win_help_src", os.F_OK | os.R_OK):
	print "Каталог win_help_src не найден"
	sys.exit(1)

# Вычитываем содержимое каталога с исходниками
list_src_file = os.listdir("win_help_src")

# Выполняем для всех типов построение каталогов с помощью
for type_pr in types:
	print "Обрабатываем для " + type_pr
	all_help = {}
	lines = open("help." + type_pr + ".src", "r").readlines()
	# Вычитываем файл конфигурации помощи
	for line in lines:
		file, text = line.split(" ", 1)
		if file not in list_src_file:
			print "Файл " + file + " не найден в списске исходных документов"
			continue
		all_help[file] = text
	if all_help == {}:
		print "Не найден ни один файл помощи для " + type_pr + ", изменения не будут произведены"
		continue
	# Удаляем старые файлы из каталога
	list_dir_file = os.listdir("win_help." + type_pr)
	for file in list_dir_file:
		if file=="CVS":
			continue
		if file+".m-k" not in all_help.keys():
			print "Удаляем из репозитория " + file
			ret = os.system("rm -r win_help." + type_pr + "/" + file + "/" + file + ".txt")
			if ret != 0:
				print "Не удалось удалить файл win_help." + type_pr + "/" + file + "/" + file + ".txt"
				sys.exit(1)
			ret = os.system("cvs remove win_help." + type_pr + "/" + file + "/" + file + ".txt")
			if ret != 0:
				print "Не удалось выполнить cvs remove для win_help." + type_pr + "/" + file + "/" + file + ".txt"
				sys.exit(1)
			ret = os.system("rm -r win_help." + type_pr + "/" + file + "/about")
			if ret != 0:
				print "Не удалось удалить файл win_help." + type_pr + "/about"
				sys.exit(1)
			ret = os.system("cvs remove win_help." + type_pr + "/" + file + "/about")
			if ret != 0:
				print "Не удалось выполнить cvs remove для win_help." + type_pr + "/" + file + "/about"
				sys.exit(1)
			ret = os.system("cvs remove win_help." + type_pr + "/" + file)
			if ret != 0:
				print "Не удалось выполнить cvs remove для win_help." + type_pr + "/" + file
				sys.exit(1)

	# Добавляем недостающие файлы и обновляем старые
	list_dir_file = os.listdir("win_help." + type_pr)
	for file in all_help.keys():
		print "\tОбрабатываем файл " + file
		new = True
		if file[:-4:] in list_dir_file:
			new = False
			ret = os.system("rm -f win_help." + type_pr + "/" + file[:-4:] + "/" + file[:-4:] + ".txt")
			if ret != 0:
				print "Не удалось удалить (для пересоздания) файл win_help." + type_pr + "/" + file[:-4:] + "/" + file[:-4:] + ".txt"
				sys.exit(1)
			ret = os.system("rm -f win_help." + type_pr + "/" + file[:-4:] + "/about")
			if ret != 0:
				print "Не удалось удалить (для пересоздания) файл win_help." + type_pr + "/" + file[:-4:] + "/about"
				sys.exit(1)
		else:
			os.mkdir("win_help." + type_pr + "/" + file[:-4:])
		# создаем файл помощи
		ret = os.system("ALDConvert win_help_src/" + file + " win_help_src/" + file[:-4:] + ".html")
		if ret!=0:
			print "Не удалось конвертировать файл win_help_src/" + file + " в html формат"
			sys.exit(1)
		ret = os.system("html2text -nobs -width 70 win_help_src/" + file[:-4:] + ".html > win_help." + type_pr + "/" + file[:-4:] + "/" + file[:-4:] + ".txt")
		if ret!=0:
			print "Не удалось конвертировать файл win_help_src/" + file + " в txt формат"
			sys.exit(1)
		ret = os.system("rm -f win_help_src/" + file[:-4:] + ".html")
		if ret!=0:
			print "Не удалось удалить файл win_help_src/" + file[:-4:] + ".html"
			sys.exit(1)
		# добавляем файл с описанием
		f = open("win_help." + type_pr + "/" + file[:-4:] + "/about", "w")
		f.write(all_help[file])
		f.write(file[:-4:] + ".txt")
		f.close()
		# Если надо, добавляем все в CVS
		if (new):
			ret = os.system("cvs add win_help." + type_pr + "/" + file[:-4:])
			if ret!=0:
				print "Не удалось добавить каталог win_help." + type_pr + "/" + file[:-4:] + " в CVS"
				sys.exit(1)
			ret = os.system("cvs add win_help." + type_pr + "/" + file[:-4:] + "/" + file[:-4:] + ".txt")
			if ret!=0:
				print "Не удалось добавить файл win_help." + type_pr + "/" + file[:-4:] + "/" + file[:-4:] + ".txt" + " в CVS"
				sys.exit(1)
			ret = os.system("cvs add win_help." + type_pr + "/" + file[:-4:] + "/about")
			if ret!=0:
				print "Не удалось добавить файл win_help." + type_pr + "/" + file[:-4:] + "/about" + " в CVS"
				sys.exit(1)
# Сохраняем все изменения
ret = os.system("cvs commit -m 'Автоматическое обновление файлов помощи'")
if ret!=0:
	print "Не удалось выполнить cvs commit"
	sys.exit(1)
# Для очистки старых каталогов
ret = os.system("cvs update")
if ret!=0:
	print "Не удалось выполнить конечный cvs update"
	sys.exit(1)





