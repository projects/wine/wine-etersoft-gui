#! /bin/sh

error()
{
	echo $1
	exit 1
}

echo "Check Python"

python -V || error "Python not install! Can not run";

echo "Start WineGUI"

name=`dirname $0`

cd $name/wineGUI/
./main.py